import React, { Component } from 'react'
import { AppRegistry, BackAndroid, BackHandler } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { MyRouter } from './src_v2/router'

console.disableYellowBox = true

const indexAndroid = () => <MyRouter />

AppRegistry.registerComponent('learnwork', () => indexAndroid)

// BackAndroid.addEventListener('hardwareBackPress', (e) => { Actions.pop(); alert('AAAAAAAAAAAAAA') })
// BackHandler.addEventListener('hardwareBackPress', () => { Actions.pop(); alert('BBBBBBBBBBBBBB') })
