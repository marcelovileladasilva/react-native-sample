import React from 'react'
import { AppRegistry } from 'react-native'
import { MyRouter } from './src_v2/router'
import { RouterPaper } from './src_v2/router.paper'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper'

console.disableYellowBox = true

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#344c84',
    primaryDark: '#344c84',
    // primaryDark: color('tomato').darken(0.2).rgb().string(),
    accent: 'yellow'
  }
}

export default class App extends React.Component {
  render () {
    // return <MyRouter />
    return (
      <PaperProvider theme={theme}>
        <RouterPaper />
      </PaperProvider>
    )
  }
}

AppRegistry.registerComponent('project', () => App)
