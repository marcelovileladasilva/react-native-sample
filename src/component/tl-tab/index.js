import React, { Component } from 'react'
import { Button, Picker, ScrollView, StyleSheet } from 'react-native'
import { View, TagsInput, TextInput, Text } from 'react-native-ui-lib'
import { ButtonGroup } from 'react-native-elements'
import { Colors, SimpleAccordion } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'

export default class TileTab extends Component {
  constructor (props) {
    super(props)
    this.state = { selectedIndex: 0 }
    this.updateIndex = this.updateIndex.bind(this)
  }

  updateIndex (selectedIndex) { this.setState({ selectedIndex }) }

  render () {
    const buttons = ['Importantes', 'Antigas', 'Normais']
    const { selectedIndex } = this.state
    return (
      <View flex-1 background-white>

        <TopHeaderSample back navigator={this.props.navigator} />

        <ScrollView>
          <View padding-20 paddingB-0>

            <View marginB-30>
              <Text h1>Abas</Text>
              <Text>Exemplos de abas</Text>
            </View>
          </View>

          <View paddingH-10>
            <ButtonGroup
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              containerStyle={{ borderColor: 'white' }}
              buttonStyle={{ backgroundColor: 'white', borderColor: 'black', borderBottomWidth: 1 }}
              innerBorderStyle={{ color: 'white' }}
              selectedTextStyle={{ color: Colors['primary'] }}
            />
          </View>

          <View padding-20 marginB-50>

            {selectedIndex === 0 &&
              <View>
                <SimpleAccordion label='Importante 1' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Importante 2' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Importante 3' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
              </View>
            }

            {selectedIndex === 1 &&
              <View>
                <SimpleAccordion label='Antiga 1' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Antiga 2' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Antiga 3' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
              </View>
            }

            {selectedIndex === 2 &&
              <View>
                <SimpleAccordion label='Normal 1' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Normal 2' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
                <SimpleAccordion label='Normal 3' opened='true'>
                  <Text>{loremIpsum}</Text>
                </SimpleAccordion>
              </View>
            }

          </View>
        </ScrollView>

      </View>
    )
  }
}

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh. Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
