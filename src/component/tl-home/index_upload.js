import React, { Component } from 'react'
import { ScrollView, Modal, Platform } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from './../../react-native-learning'
import { TopHeaderSample, Transfer } from './../shared'

export default class TileHome extends Component {
  state = { file: undefined, response: undefined }

  async fileHandler() {
    t = await new Transfer()
    t.selectFileTapped(this.selectFileTappedCallBack.bind(this))
  }

  async selectFileTappedCallBack(r) {
    this.setState({file: r})
    if (!r.cancelled) {
      this.upload()
    }
  }

  async upload() {
    t = await new Transfer()
    const response = await t.upload(this.state.file)
    this.setState({ response })
  }

  render() {
    return (
      <View flex-1 background-white>
        <TopHeaderSample back navigator={this.props.navigator} />
        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            <Button size='small' background-primary label='Selecionar arquivo' onPress={this.fileHandler.bind(this)} />

            <Text>Dados obtidos ao ler arquivo localmente:</Text>
            <Text>{JSON.stringify(this.state.file)}</Text>

            <Text>Resposta da requisicao:</Text>
            <Text>{JSON.stringify(this.state.response)}</Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}
