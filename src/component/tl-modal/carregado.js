import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, TextInput } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from './../../react-native-learning'

export default class ModalCarregado extends Component {
  render () {
      return (
          <View>
              <View row spread>
                  <Text text70>Modal com conteúdo carregado</Text>
                  <TouchableOpacity onPress={() => { this.props.close() }}>
                      <Icon name='close' size={24} color={'grey'} ref={this.onRef} />
                    </TouchableOpacity>
                </View>
              <Text>Tem certeza que deseja fazer isso?</Text>

              <TextInput
                  style={{ height: 40 }}
                  placeholder='Email'
                  onChangeText={(text) => { }}
                />

              <TextInput
                  style={{ height: 40 }}
                  placeholder='Segurança'
                  onChangeText={(text) => { }}
                />

              <View left marginT-10>
                  <Button size='small' background-primary label='Salvar'
                      onPress={() => { this.props.save() }} />
                </View>
            </View>
        )
    }
}

const styles = {
  wellBox: {
      borderColor: Colors['dark30'],
      borderWidth: 1,
      borderLeftWidth: 5,
      padding: 10
    }
}
