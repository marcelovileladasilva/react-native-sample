import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'
import ModalCarregado from './carregado'

export default class TileModal extends Component {
  constructor (props) {
    super(props)
    this.state = { modalVisible: false, modalContent: '' }
  }

  render () {
    return (
      <View flex-1 background-white>

        <TopHeaderSample back navigator={this.props.navigator} />

        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            <View marginB-30>
              <Text h1>Modais</Text>
              <Text>Exemplos de modais</Text>
            </View>

            {this.renderEstatico()}

            <View marginB-10 style={styles.wellBox}>
              <Text text70>Modais</Text>
              <Text>Use os modais para exibir muita informação sobreposta na tela, sem precisar redirecionar.</Text>

              <View>
                <View marginB-10 />
                <Button size='small' background-primary label='Conteúdo estático'
                  onPress={() => { this.setState({ modalVisible: true, modalContent: 'estatico' }) }}
                />
                <View marginB-10 />
                <Button size='small' background-primary label='Conteúdo carregado'
                  onPress={() => { this.setState({ modalVisible: true, modalContent: 'carregado' }) }}
                />
              </View>
            </View>

          </View>
        </ScrollView>
      </View>
    )
  }

  renderEstatico () {
    return (
      <Modal
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
        transparent
      >
        <View centerV flex style={{ backgroundColor: '#FFFFFF50', marginTop: 50 }}>
          <View background-white marginH-20 style={styles.wellBox}>
            {this.state.modalContent === 'estatico' && this.renderModalEstatico()}
            {this.state.modalContent === 'carregado' && this.renderModalCarregado()}
          </View>
        </View>
      </Modal >
    )// https://stackoverflow.com/questions/36147082/react-native-style-opacity-for-parent-and-child
  }

  renderModalEstatico () {
    return (
      <View>
        <View row spread>
          <Text text70>Confirme o que deseja fazer</Text>
          <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
            <Icon name='close' size={24} color={'grey'} ref={this.onRef} />
          </TouchableOpacity>
        </View>
        <Text>Tem certeza que deseja fazer isso?</Text>

        <View left marginT-10>
          <Button size='small' background-primary label='Salvar'
            onPress={() => { this.setState({ modalVisible: false }) }} />
        </View>
      </View>
    )
  }

  renderModalCarregado () {
    return (
      <ModalCarregado
        close={() => { this.setState({ modalVisible: false }) }}
        save={() => { this.setState({ modalVisible: false }) }}
      />
    )
  }
}

const styles = {
  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}
