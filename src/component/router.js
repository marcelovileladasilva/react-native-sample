import React, { Component } from 'react'
import { View, BackAndroid } from 'react-native'
// import { Navigator } from 'react-native-deprecated-custom-components'
import { Router, Scene, Stack, Actions } from 'react-native-router-flux'
import { Theme } from './../config/theme.js'

import TileSearch from './tl-search'
import TileTable from './tl-table'
import TileWell from './tl-well'
import TileModal from './tl-modal'
import TileTab from './tl-tab'
import TileMessage from './tl-message'
import TileHome from './tl-home'
/* import TileMapStatic from './tl-geo/1_static'
import TileMapState from './tl-geo/2_state'
import TileMapMarker from './tl-geo/3_marker'
import TileMapDirections from './tl-geo/4_directions'
import TileMapPolyline from './tl-geo/5_polyline'
import TileGeolocation from './tl-geo/0_geo'
import TileGeomap from './tl-geo/01_geomap' */
import TileFilepicker from './tl-home/index_upload'

// import TileHomeHG from './src/component/tl-home/tl-home'

global.expo = true

//
// https://stackoverflow.com/questions/34496246/handling-back-button-in-react-native-navigator-on-android
export class MyRouter extends Component {
  constructor (props) {
    super(props)
    this.handleBack = this.handleBack.bind(this)
  }

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this.handleBack)
  }

  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBack)
  }

  handleBack () { // alert(JSON.stringify(Actions))
    if (Actions.reducer !== null) {
      Actions.pop()
    }
  }

  render () {
    this.handleBack()

    if (!global.expo) {
      return (// onExitApp={() => { alert('Sair') }}
        <Router hideNavBar>
          <Stack key='root' hideNavBar>
            <Scene key='home' component={TileHome} hideNavBar />
            <Scene key='message' component={TileMessage} title='Mensagens' hideNavBar />
            <Scene key='search' component={TileSearch} title='Busca' hideNavBar />
            <Scene key='tab' component={TileTab} title='Abas' hideNavBar />
            <Scene key='table' component={TileTable} title='Tabelas' hideNavBar />
            <Scene key='well' component={TileWell} title='Caixas' hideNavBar />
            <Scene key='modal' component={TileModal} title='Modais' hideNavBar />
            <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
            {/* <Scene key='mapstatic' component={TileMapStatic} title='Estático' hideNavBar />
            <Scene key='mapstate' component={TileMapState} title='Estado' hideNavBar />
            <Scene key='mapmarker' component={TileMapMarker} title='Marcador' hideNavBar />
            <Scene key='mapdirections' component={TileMapDirections} title='Rota - Directions' hideNavBar />
            <Scene key='mappolyline' component={TileMapPolyline} title='Rota - Polyline' hideNavBar />
            <Scene key='geolocation' component={TileGeolocation} title='Rota - Polyline' hideNavBar />
            <Scene key='geomap' component={TileGeomap} title='Rota - Polyline' hideNavBar /> */}
          </Stack>
        </Router>
      )
    } else {
      return (// onExitApp={() => { alert('Sair') }}
        <Router hideNavBar>
          <Stack key='root' hideNavBar>
            <Scene key='home' component={TileHome} hideNavBar />
            <Scene key='message' component={TileMessage} title='Mensagens' hideNavBar />
            <Scene key='search' component={TileSearch} title='Busca' hideNavBar />
            <Scene key='tab' component={TileTab} title='Abas' hideNavBar />
            <Scene key='table' component={TileTable} title='Tabelas' hideNavBar />
            <Scene key='well' component={TileWell} title='Caixas' hideNavBar />
            <Scene key='modal' component={TileModal} title='Modais' hideNavBar />
            <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
            {/*
          <Scene key='mapstatic' component={TileMapStatic} title='Estático' hideNavBar />
          <Scene key='mapstate' component={TileMapState} title='Estado' hideNavBar />
          <Scene key='mapmarker' component={TileMapMarker} title='Marcador' hideNavBar />
          <Scene key='mapdirections' component={TileMapDirections} title='Rota - Directions' hideNavBar />
          <Scene key='mappolyline' component={TileMapPolyline} title='Rota - Polyline' hideNavBar />
          <Scene key='geolocation' component={TileGeolocation} title='Rota - Polyline' hideNavBar />
          <Scene key='geomap' component={TileGeomap} title='Rota - Polyline' hideNavBar />
          */}
          </Stack>
        </Router>
      )
    }
  }
}

export const Router_ = () => <TileModal />

export const Router_Navigator = () =>
  <View>
    <Navigator
      initialRoute={{ id: 'home' }}
      renderScene={(route, navigator) => {
        console.log('Rota')
        console.log(route)
        switch (route.id) {
          case 'home':
            return (<TileHome navigator={navigator} />)

          // case 'detail':
          // return (<TileDetail navigator={navigator} post={route.post} />)

          case 'message':
            return (<TileMessage navigator={navigator} />)

          case 'search':
            return (<TileSearch navigator={navigator} />)

          case 'tab':
            return (<TileTab navigator={navigator} />)

          case 'table':
            return (<TileTable navigator={navigator} />)

          case 'well':
            return (<TileWell navigator={navigator} />)

          case 'modal':
            return (<TileModal navigator={navigator} />)

          case 'mapstatic':
            return (<TileMapStatic navigator={navigator} />)

          case 'mapstate':
            return (<TileMapState navigator={navigator} />)

          case 'mapmarker':
            return (<TileMapMarker navigator={navigator} />)

          case 'mapdirections':
            return (<TileMapDirections navigator={navigator} />)

          case 'mappolyline':
            return (<TileMapPolyline navigator={navigator} />)

          default:
            return false
        }
      }}
    />
  </View>
