import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native'
import { Colors } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'
import MapView from 'react-native-maps'
import Polyline from '@mapbox/polyline'
import directions from './../../static-data/directions'

// Rota(gerando caminho atraves do Google maps direction)
// https://github.com/react-community/react-native-maps/issues/778
// Rota(exibindo caminho atraves do maps polyline)
// https://medium.com/@ali_oguzhan/react-native-maps-with-google-directions-api-bc716ed7a366
module.exports = class TileHome extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      coords: []
    }
  }

  componentDidMount () {
    // find your origin and destination point coordinates and pass it to our method.
    // I am using Bursa,TR -> Istanbul,TR for this example
    this.getDirections('40.1884979, 29.061018', '41.0082,28.9784')
  }

  async getDirections (startLoc, destinationLoc) {
    const GOOGLE_API_KEY = 'AIzaSyBNOJQsoYk1-_ba7jN6EliHpuCp1Nqz1bo'
    try {
      // let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc}&departure_time=${Date.now()}&traffic_model=best_guess&key=${GOOGLE_API_KEY}`)
      // let respJson = await resp.json()
      let points = Polyline.decode(directions.routes[ 0 ].overview_polyline.points)
      let coords = points.map((point, index) => {
        return {
          latitude: point[ 0 ],
          longitude: point[ 1 ]
        }
      })
      this.setState({ coords: coords })
      return coords
    } catch (error) {
      alert(error)
      return error
    }
  }

  render () {
    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' navigator={this.props.navigator} />
        <View>
          <MapView style={styles.map} initialRegion={{
            latitude: 42.642312,
            longitude: 18.111793,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }}>

            <MapView.Polyline
              coordinates={this.state.coords}
              strokeWidth={2}
              strokeColor='red' />

          </MapView>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
})
