import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, StyleSheet } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'

const FilePickerManager = require('NativeModules').FilePickerManager
//import RNFetchBlob from 'react-native-fetch-blob'
import Upload from 'react-native-background-upload'

import MapView from 'react-native-maps'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 600,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
})

module.exports = class TileGeomap extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      latitude: null, // -22.901
      longitude: null, // -43.174
      error: null
    }
  }

  async componentDidMount () {
    // await perm()
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        // alert(position)
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        })
      },
      (error) => { /* alert(error); */; this.setState({ error: error.message }) },
      { timeout: 20000, maximumAge: 1000, distanceFilter: 10, enableHighAccuracy: false }
    )
  }

  componentWillUnmount () {
    navigator.geolocation.clearWatch(this.watchId)
  }

  render () {
    const { region } = this.props
    console.log(region)

    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' navigator={this.props.navigator} />
        <ScrollView>
          <View height={250}>
            <View style={styles.container}>
              {this.state.latitude && this.state.longitude &&
                <MapView
                  style={styles.map}
                  region={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121
                  }}
                // https://www.google.com/maps/@56.1858015,-4.0499006,3a,75y,195.32h,90t/data=!3m7!1e1!3m5!1s-IawbNZ_AzkScyW_YzqvgQ!2e0!3e5!7i13312!8i6656
                >
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.latitude,
                      longitude: this.state.longitude
                    }}
                    title={'Estou aqui'}
                    description={`Latitude: ${this.state.latitude}, Longitude: ${this.state.longitude}`}
                  />
                </MapView>
              }
            </View>
          </View>
          <View padding-20 paddingB-0 marginB-100 />
        </ScrollView>
      </View>
    )
  }
}

// https://g1.globo.com/tecnologia/noticia/google-cria-guia-virtual-das-locacoes-de-game-of-thrones.ghtml
// https://www.google.com/streetview/#game-of-thrones-locations/
// http://www.pipocaerefri.com.br/2017/07/03/fa-cria-mapa-com-localizacao-e-ordem-cronologica-do-universo-cinematografico-marvel/
// https://omelete.uol.com.br/series-tv/noticia/the-walking-dead-mapa-mostra-onde-ficariam-os-principais-locais-da-serie-no-mundo-real/

/*
daenerys-and-khal-drogo-wedding
https://www.google.com/maps/@36.052371,14.189879,3a,75y,259.53h,90t/data=!3m5!1e1!3m3!1sXHYdnc3I4Hau8OKlhMhQEg!3e5!2e0
latitude: 36.052371,
longitude: 14.189879,
==============================
end-of-cerseis-walk-of-shame
https://www.google.com/maps/@42.642312,18.111793,3a,75y,169.88h,90t/data=!3m5!1e1!3m3!1sKiH-B5ImZDt05LluI9xiCA!3e5!2e0
latitude: 42.642312,
longitude: 18.111793,
==============================
winterfell
latitude: 56.1858015,
longitude: -4.0499006,
==============================
the-iron-islands
https://www.google.com/maps/@55.212463,-6.575143,3a,75y,260.87h,90t/data=!3m5!1e1!3m3!1sta9rbxCH_45GCWAMdNbkcw!3e5!2e0
*/
