import { DocumentPicker } from 'expo'
export class ExpoPicker {
  selectFileTapped (fn) {
    let response = { cancelled: true }

    DocumentPicker.getDocumentAsync({}).then((r) => {
      if (!r.cancelled) {
        r.fileName = r.name
        r.path = r.uri
        response = r
        fn(response)
      }
    })
  }
}
