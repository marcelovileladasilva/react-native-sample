import React, { Component } from 'react'
import { ScrollView, TouchableOpacity, AsyncStorage } from 'react-native'
import { Text, View } from 'react-native-ui-lib'
import { Colors, TopHeader, NavParent, NavChild, PopupMenu } from './../../react-native-learning'
import { Actions } from 'react-native-router-flux'

export class TopHeaderSample extends Component {
  constructor (props) {
    super(props)
    this.state = { sideMenuVisible: false }
    this.onPopupEvent = this.onPopupEvent.bind(this)
    this.toogleMenu = this.toogleMenu.bind(this)
  }

  toogleMenu (BoShow) {
    this.setState({ sideMenuVisible: BoShow })
  }

  render () {
    // if (this.state.sideMenuVisible) {
    return (
      <TopHeader {...this.props} sideMenuVisible={this.state.sideMenuVisible} toogleMenu={this.toogleMenu}>
        {
          this.renderContent()
        }
      </TopHeader>
    )
    // }
    return false
  }

  renderContent () {
    const StEmail = this.state.profileLoaded ? this.profile.StEmail : ''
    // GREEN50
    const n = { navigator: this.props.navigator }
    return (
      <View flex-1 background-white paddingL-20>
        <ScrollView>
          <View row spread centerV paddingT-20>
            <Text>padrão@hostnet.com.br</Text>
            <PopupMenu actions={[ 'Conta', 'Ajuda', 'Sair' ]} onPress={this.onPopupEvent} />
          </View>

          <View marginB-10 />

          <NavParent label='Atendimento'>
            <NavChild label='Central de atendimento' />
            <NavChild label='Wiki Hostnet' />
            <NavChild label='2ª via boleto' />
          </NavParent>

          <NavParent label='Sistemas'>
            <NavChild label='Hostnet' />
            <NavChild label='Ultramail' />
            <NavChild label='Painel' />
            <NavChild label='Apps' />
            <NavChild label='Webfácil' />
            <NavChild label='Drive' />
          </NavParent>

          {this.renderNavChild('Formulário', 'home')}
          {this.renderNavChild('Tabela', 'table')}
          {this.renderNavChild('Caixas', 'well')}
          {this.renderNavChild('Abas', 'tab')}
          {this.renderNavChild('Erros', 'message')}

          <NavParent label='Opções'>
            {this.renderNavChild('Modais', 'modal')}
            <NavChild label='Menu flutuante' />
            <NavChild label='Carregamento' />

            <NavParent label='Mais opções'>
              <NavChild label='Outro link direto' />
              <NavChild label='Mais opções' />
              {this.renderNavChild('File Picker', 'filepicker')}
              {!global.expo &&
                <View>
                  {this.renderNavChild('Estático', 'mapstatic')}
                  {this.renderNavChild('Estado', 'mapstate')}
                  {this.renderNavChild('Marcador', 'mapmarker')}
                  {this.renderNavChild('Rota - Directions', 'mapdirections')}
                  {this.renderNavChild('Rota - Polyline', 'mappolyline')}
                  {this.renderNavChild('Geolocation', 'geolocation')}
                  {this.renderNavChild('Geo + mapa', 'geomap')}
                </View>
              }
            </NavParent>

            <NavChild label='Link direto' />
            <NavChild label='Link direto' />
          </NavParent>
        </ScrollView>
      </View>
    )
  }

  renderNavChild (label, route, routeParams = {}) {
    // <NavChild label='Início' route='homehg' {...n} />
    return (
      <TouchableOpacity onPress={() => {
        if (route) {
          // this.setState({ sideMenuVisible: false })
          this.toogleMenu(false)
          // alert(JSON.stringify({ id: route, ...routeParams }))
          // this.props.navigator.push({ id: route, ...routeParams })
          Actions[ route ](routeParams)
        }
      }}>
        <NavChild label={label} />
      </TouchableOpacity>
    )
  }

  onPopupEvent (eventName, index) {
    // alert(`${eventName} - ${index}`)
    if (eventName !== 'itemSelected') return
    if (index === 0) alert('Comportamento - Conta')
    if (index === 1) alert('Comportamento - Ajuda')
    if (index === 2) alert('Comportamento - Sair')
  }
}

/* const onPopupEvent = async (eventName, index) => {
  // alert(`${eventName} - ${index}`)
  if (eventName !== 'itemSelected') return
  if (index === 0) alert('Comportamento - Conta')
  if (index === 1) alert('Comportamento - Ajuda')
  //if (index === 2) alert('Comportamento - Sair')
  if (index === 2) {
    await AsyncStorage.setItem('token', '')
    this.props.navigator.push({ id: 'home' })
  }
} */

const styles = {
  bold: {
    fontWeight: 'bold'
  },
  divider: {
    borderColor: Colors[ 'dark70' ],
    borderWidth: 0,
    borderTopWidth: 2,
    paddingTop: 15,
    paddingBottom: 5,
    marginTop: 10
  }
}
