import React, { Component } from 'react'
// import { AppRegistry, Alert, Button, View } from 'react-native'
// import { ButtonGroup, Text } from 'react-native-elements'
import { Button, Picker, ScrollView, StyleSheet } from 'react-native'
import { View, TagsInput, TextInput, Text, Colors } from 'react-native-ui-lib'
import { Icon } from 'react-native-elements'
import { RequestResponse } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'

export default class TileMessage extends Component {
  constructor (props) {
    super(props)
    this.loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis
    auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque
    tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque
    pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis
    metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis
    dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh.
    Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
    this.state = {
      selectedCase: 'none'
    }
    this.updateState = this.updateState.bind(this)
  }
  updateState (selectedCase) {
    this.setState({ selectedCase })
  }

  render () {
    const { selectedCase } = this.state
    return (
      <View flex-1 background-white>

        <TopHeaderSample back navigator={this.props.navigator} />

        <ScrollView>
          <View padding-20>
            <View marginB-30>
              <Text h1>Mensagens de erro/sucesso</Text>
              <Text>Exemplos de mensagens</Text>
            </View>

            {selectedCase === 'success' &&
              <RequestResponse success={['Operação realizada com sucesso']} />
            }
            {selectedCase === 'error' &&
              <RequestResponse error={['Mensagem de erro com tela carregada']} />
            }

            <View row spread marginB-10>
              <Text>Erro em tela carregada</Text>
              <Text onPress={() => this.updateState('error')}>[Link]</Text>
            </View>

            <View row spread marginB-10>
              <Text>Erro fatal sem tela carregada</Text>
              <Text>[Link]</Text>
            </View>

            <View row spread marginB-10>
              <Text>Sucesso em tela carregada</Text>
              <Text onPress={() => this.updateState('success')}>[Link]</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}
