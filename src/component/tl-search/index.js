import React, { Component } from 'react'
import { AppRegistry, Alert, Button, View } from 'react-native'
import { ButtonGroup, Text, Card } from 'react-native-elements'
import SearchResults from './searchResults'
import { TreatList } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'

export default class TileSearch extends Component {
  constructor (props) {
    super(props)
    this.ArOpcoes = [
      { 'StTitulo': 'Funcionário', 'StSubtitulo1': 'Corporate', 'StSubTitulo2': 'Opção', 'Link': '#' },
      { 'StTitulo': 'Funcionamento do tópico', 'StSubtitulo1': 'HelpGroup', 'StSubTitulo2': 'Tópico', 'Link': '#' },
      { 'StTitulo': 'Funcionários do DEV de ponto', 'StSubtitulo1': 'HelpGroup', 'StSubTitulo2': 'Grupo', 'Link': '#' },
      { 'StTitulo': 'Roberto Confuncio', 'StSubtitulo1': 'Corporate', 'StSubTitulo2': 'Cliente', 'Link': '#' }
    ]
    this.state = {
      text: '', page: 0, orderColumn: 'StTitulo', orderDirection: 'asc'
    }
    this.updateState = this.updateState.bind(this)
  }

  updateState (selectedCase) { this.setState({ text: selectedCase }) }

  render () {
    const s = this.state
    t = new TreatList()
    columns = ['StTitulo', 'StSubtitulo1', 'StSubTitulo2', 'Link']
    listTreated = t.getTreatedItens(this.ArOpcoes, s.text, columns, s.page, s.orderColumn, s.orderDirection)

    return (
      <View flex-1 background-white>
        <TopHeaderSample
          search
          filterText={this.state.filterText}
          callback={this.updateState} />
        <SearchResults results={listTreated.data} />
      </View>
    )
  }
}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}

/*
const caixas = render__() {
  const { selectedCase } = this.state
  alert(DfStyle.bgPrimary)
      //bgPrimary: { backgroundColor: variables.dfBrandPrimary },
      //bgSecondary: { backgroundColor: variables.dfBrandPrimary },
      //bgSuccess: { backgroundColor: variables.df_brand_success },
      //bgDanger: { backgroundColor: variables.df_brand_error },
      //bgWarning: { backgroundColor: variables.df_brand_warn },
      //bgInfo: { backgroundColor: variables.df_brand_info },
      //bgLight: { backgroundColor: variables.dfBrandPrimary },
      //bgDark: { backgroundColor: variables.df_brand_component_disabled },

  return (
    <View>

      <Card title="AAAbcdefrr" style={DfStyle.bgPrimary}>
        <Text style={DfStyle.fcPrimary}>{DfStyle.fcPrimary}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgSuccess}>
        <Text>{DfStyle.fcPrimary}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgDanger}>
        <Text>{DfStyle.fcPrimary}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgWarning}>
        <Text>{DfStyle.fcPrimary}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgInfo}>
        <Text>{DfStyle.bgInfo}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgLight}>
        <Text>{DfStyle.fcPrimary}</Text>
      </Card>

      <Card title="AAAbcdef" style={DfStyle.bgDark}>
        <Text>{DfStyle.fcPrimary}</Text>
      </Card>

    </View>
  )
}
*/
