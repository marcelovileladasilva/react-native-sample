import React from 'react'
import { ScrollView } from 'react-native'
import { Button, View, Text, Colors } from 'react-native-ui-lib'
import { Divider } from 'react-native-elements'
import { SimpleAccordion } from './../../react-native-learning'
import { TopHeaderSample } from './../shared'

export default props => {
  return (
    <View flex-1 background-white>
      <TopHeaderSample back navigator={props.navigator} />

      <ScrollView>
        <View padding-20 marginB-50>

          <View marginB-30>
            <Text h1>Caixas</Text>
            <Text>Exemplos de caixas de informação</Text>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <Text>meusite.com</Text>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <Text>Meu site</Text>
            <Text>meusite.com</Text>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <View row spread centerV>
              <Text>meusite.com</Text>
              <Button size='small' background-primary label='Administrar' onPress={() => { }} />
            </View>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <View row spread centerV>
              <View>
                <Text>Meu site</Text>
                <Text>meusite.com</Text>
              </View>
              <Button size='small' background-primary label='Administrar' onPress={() => { }} />
            </View>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <Text>contato@meusite.com</Text>
            <Text>12,3GB de 40GB</Text>
            <Text>Criado em 10/07/2012</Text>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <Text>Renovação dominio.com - 1 ano(s)</Text>
            <View row spread>
              <View>
                <Text>R$ 49,90</Text>
                <Text>Valor</Text>
              </View>
              <View>
                <Text>email@meusite.com</Text>
                <Text>Email</Text>
              </View>
              <View>
                <Text>site123.net</Text>
                <Text>Cliente</Text>
              </View>
            </View>
          </View>

          <View marginB-10 style={styles.wellBox}>
            <View row spread paddingB-10>
              <Text>Meu site</Text>
              <Text>meusite.com</Text>
            </View>
            <Divider />
            <View row spread paddingV-10 centerV>
              <View>
                <Text>Meu site</Text>
                <Text>meusite.com</Text>
              </View>
              <Button size='small' background-primary label='Administrar' onPress={() => { }} />
            </View>
            <Divider />
            <View row spread paddingT-10>
              <View>
                <Text>Meu site</Text>
                <Text>12,3GB de 40GB</Text>
              </View>
              <Text>Criado em 10/07/2012</Text>
            </View>
          </View>

          <View marginB-30 />

          <View marginB-30>
            <Text text60 blue30>Caixas separadoras</Text>
            <Text>Exemplos de caixas separadoras</Text>
          </View>

          <SimpleAccordion label='Caixa 1' opened>
            <Text>{loremIpsum}</Text>
          </SimpleAccordion>
          <SimpleAccordion label='Caixa 2222' opened>
            <Text>{loremIpsum}</Text>
          </SimpleAccordion>

        </View>
      </ScrollView>

    </View>
  )
}

const styles = {
  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh. Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
