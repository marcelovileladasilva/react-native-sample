import { Theme } from './asset/theme.js'
import React, { Component } from 'react'
import { BackAndroid } from 'react-native'
// import { Navigator } from 'react-native-deprecated-custom-components'
import { Router, Scene, Actions } from 'react-native-router-flux'
import SideMenuSample from './component/SideMenuSample.paper'

import Busca from './view.paper/busca'
import Tabela from './view.paper/tabela'
import Caixa from './view.paper/caixa'
import Modal from './view.paper/modal'
import Aba from './view.paper/aba'
import Mensagem from './view.paper/mensagem'
import Inicial from './view.paper/formulario'
import TileFilepicker from './view.paper/formulario/index_upload'
/* import TileMapStatic from './view.paper/geo/1_static'
import TileMapState from './view.paper/geo/2_state'
import TileMapMarker from './view.paper/geo/3_marker'
import TileMapDirections from './view.paper/geo/4_directions'
import TileMapPolyline from './view.paper/geo/5_polyline'
import TileGeolocation from './view.paper/geo/0_geo'
import TileGeomap from './view.paper/geo/01_geomap'
*/

global.expo = true

export class RouterPaper extends Component {
  constructor (props) {
    super(props)
    this.handleBack = this.handleBack.bind(this)
  }

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this.handleBack)
  }

  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBack)
  }

  handleBack () { // alert(JSON.stringify(Actions))
    // https://stackoverflow.com/questions/34496246/handling-back-button-in-react-native-navigator-on-android
    if (Actions.reducer !== null) {
      Actions.pop()
    }
  }

  render () {
    this.handleBack()

    // Paper
    if (!global.expo) {
      return (
        <Router hideNavBar >
          <Scene drawer key='drawer' contentComponent={SideMenuSample}>
            <Scene key='inicial' component={Inicial} hideNavBar initial />
            <Scene key='mensagem' component={Mensagem} title='Mensagens' hideNavBar />
            <Scene key='busca' component={Busca} title='Busca' hideNavBar />
            <Scene key='aba' component={Aba} title='Abas' hideNavBar />
            <Scene key='tabela' component={Tabela} title='Tabelas' hideNavBar />
            <Scene key='caixa' component={Caixa} title='Caixas' hideNavBar />
            <Scene key='modal' component={Modal} title='Modais' hideNavBar />
            <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
            {/*
              <Scene key='mapstatic' component={TileMapStatic} title='Estático' hideNavBar />
              <Scene key='mapstate' component={TileMapState} title='Estado' hideNavBar />
              <Scene key='mapmarker' component={TileMapMarker} title='Marcador' hideNavBar />
              <Scene key='mapdirections' component={TileMapDirections} title='Rota - Directions' hideNavBar />
              <Scene key='mappolyline' component={TileMapPolyline} title='Rota - Polyline' hideNavBar />
              <Scene key='geolocation' component={TileGeolocation} title='Rota - Polyline' hideNavBar />
              <Scene key='geomap' component={TileGeomap} title='Rota - Polyline' hideNavBar />
            */}
          </Scene>
        </Router>
      )
    } else {
      return (// onExitApp={() => { alert('Sair') }}
        <Router hideNavBar >
          <Scene drawer key='drawer' contentComponent={SideMenuSample}>
            <Scene key='inicial' component={Inicial} hideNavBar initial />
            <Scene key='mensagem' component={Mensagem} title='Mensagens' hideNavBar />
            <Scene key='busca' component={Busca} title='Busca' hideNavBar />
            <Scene key='aba' component={Aba} title='Abas' hideNavBar />
            <Scene key='tabela' component={Tabela} title='Tabelas' hideNavBar />
            <Scene key='caixa' component={Caixa} title='Caixas' hideNavBar />
            <Scene key='modal' component={Modal} title='Modais' hideNavBar />
            <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
          </Scene>
        </Router>
      )
    }
  }
}
