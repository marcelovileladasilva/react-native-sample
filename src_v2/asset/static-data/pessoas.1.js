const data = [
  {
    'StUsuario': 'Henrique',
    'StAcao': 'Adicionar',
    'StObjeto': '/usuario/imagem.jpg',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Subiu novo arquivo',
    'StData': '29/05/2015 12:00:00'
  },
  {
    'StUsuario': 'Welton',
    'StAcao': 'Adicionar',
    'StObjeto': '/usuario/imagem.jpg',
    'StDispositivo': 'iOS (iPad Trabalho)',
    'StDescricao': 'Salvou novo arquivo',
    'StData': '29/05/2015 12:00:01'
  },
  {
    'StUsuario': 'Eudes',
    'StAcao': 'Compartilhar',
    'StObjeto': '/usuario/imagem.jpg',
    'StDispositivo': 'Windows XP (Casa)',
    'StDescricao': 'Compartilhou com Daniel',
    'StData': '29/05/2015 12:00:02'
  },
  {
    'StUsuario': 'Thiago',
    'StAcao': 'Deletar',
    'StObjeto': '/usuario/pasta/index.html',
    'StDispositivo': 'Windows XP (Casa)',
    'StDescricao': 'Deletou arquivo',
    'StData': '29/05/2015 12:00:03'
  },
  {
    'StUsuario': 'Vitor',
    'StAcao': 'Deletar',
    'StObjeto': 'Lixeira',
    'StDispositivo': 'Windows8 (PC)',
    'StDescricao': 'Limpou lixeira',
    'StData': '29/05/2015 12:00:04'
  },
  {
    'StUsuario': 'Luciano',
    'StAcao': 'Editar',
    'StObjeto': '/usuario/pasta/index.html',
    'StDispositivo': 'Windows8 (PC)',
    'StDescricao': 'Editou arquivo',
    'StData': '29/05/2015 12:00:05'
  },
  {
    'StUsuario': 'Daniel',
    'StAcao': 'Login',
    'StObjeto': '-',
    'StDispositivo': 'iOS (iPad Trabalho)',
    'StDescricao': 'Fez login',
    'StData': '29/05/2015 12:00:06'
  },
  {
    'StUsuario': 'Luciano',
    'StAcao': 'Sincronizar',
    'StObjeto': '-',
    'StDispositivo': 'iOS (iPad Trabalho)',
    'StDescricao': 'Sincronizou com device',
    'StData': '29/05/2015 12:00:07'
  },
  {
    'StUsuario': 'Bruno',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  },
  {
    'StUsuario': 'AAA',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  },
  {
    'StUsuario': 'BBB',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  },
  {
    'StUsuario': 'CCC',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  },
  {
    'StUsuario': 'DDD',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  },
  {
    'StUsuario': 'EEE',
    'StAcao': 'Visualizar',
    'StObjeto': 'Usuários',
    'StDispositivo': 'Android (Casa)',
    'StDescricao': 'Visualizou opção',
    'StData': '29/05/2015 12:00:08'
  }
]

export default data
