import React, { Component } from 'react'
import { View, BackAndroid } from 'react-native'
// import { Navigator } from 'react-native-deprecated-custom-components'
import { Router, Scene, Stack, Actions } from 'react-native-router-flux'
import { Theme } from './asset/theme.js'

import Busca from './view/busca'
import Tabela from './view/tabela'
import Caixa from './view/caixa'
import Modal from './view/modal'
import Aba from './view/aba'
import Mensagem from './view/mensagem'
import Inicial from './view/formulario'
import TileMapStatic from './view/geo/1_static'
import TileMapState from './view/geo/2_state'
import TileMapMarker from './view/geo/3_marker'
import TileMapDirections from './view/geo/4_directions'
import TileMapPolyline from './view/geo/5_polyline'
import TileGeolocation from './view/geo/0_geo'
import TileGeomap from './view/geo/01_geomap'
import TileFilepicker from './view/formulario/index_upload'

// import TileHomeHG from './src/component/tl-home/tl-home'

global.expo = false

//
// https://stackoverflow.com/questions/34496246/handling-back-button-in-react-native-navigator-on-android
export class MyRouter extends Component {
  constructor (props) {
    super(props)
    this.handleBack = this.handleBack.bind(this)
  }

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this.handleBack)
  }

  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBack)
  }

  handleBack () { // alert(JSON.stringify(Actions))
    if (Actions.reducer !== null) {
      Actions.pop()
    }
  }

  render () {
    this.handleBack()

    if (!global.expo) {
      return (// onExitApp={() => { alert('Sair') }}
        <Router hideNavBar>
            <Stack key='root' hideNavBar>
                <Scene key='inicial' component={Inicial} hideNavBar />
                <Scene key='mensagem' component={Mensagem} title='Mensagens' hideNavBar />
                <Scene key='busca' component={Busca} title='Busca' hideNavBar />
                <Scene key='aba' component={Aba} title='Abas' hideNavBar />
                <Scene key='tabela' component={Tabela} title='Tabelas' hideNavBar />
                <Scene key='caixa' component={Caixa} title='Caixas' hideNavBar />
                <Scene key='modal' component={Modal} title='Modais' hideNavBar />
                <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
                <Scene key='mapstatic' component={TileMapStatic} title='Estático' hideNavBar />
                <Scene key='mapstate' component={TileMapState} title='Estado' hideNavBar />
                <Scene key='mapmarker' component={TileMapMarker} title='Marcador' hideNavBar />
                <Scene key='mapdirections' component={TileMapDirections} title='Rota - Directions' hideNavBar />
                <Scene key='mappolyline' component={TileMapPolyline} title='Rota - Polyline' hideNavBar />
                <Scene key='geolocation' component={TileGeolocation} title='Rota - Polyline' hideNavBar />
                <Scene key='geomap' component={TileGeomap} title='Rota - Polyline' hideNavBar />
              </Stack>
          </Router>
            )
    } else {
      return (// onExitApp={() => { alert('Sair') }}
        <Router hideNavBar>
            <Stack key='root' hideNavBar>
                <Scene key='inicial' component={Inicial} hideNavBar />
                <Scene key='mensagem' component={Mensagem} title='Mensagens' hideNavBar />
                <Scene key='busca' component={Busca} title='Busca' hideNavBar />
                <Scene key='aba' component={Aba} title='Abas' hideNavBar />
                <Scene key='tabela' component={Tabela} title='Tabelas' hideNavBar />
                <Scene key='caixa' component={Caixa} title='Caixas' hideNavBar />
                <Scene key='modal' component={Modal} title='Modais' hideNavBar />
                <Scene key='filepicker' component={TileFilepicker} title='File Picker' hideNavBar />
                {/*
          <Scene key='mapstatic' component={TileMapStatic} title='Estático' hideNavBar />
          <Scene key='mapstate' component={TileMapState} title='Estado' hideNavBar />
          <Scene key='mapmarker' component={TileMapMarker} title='Marcador' hideNavBar />
          <Scene key='mapdirections' component={TileMapDirections} title='Rota - Directions' hideNavBar />
          <Scene key='mappolyline' component={TileMapPolyline} title='Rota - Polyline' hideNavBar />
          <Scene key='geolocation' component={TileGeolocation} title='Rota - Polyline' hideNavBar />
          <Scene key='geomap' component={TileGeomap} title='Rota - Polyline' hideNavBar />
          */}
              </Stack>
          </Router>
            )
    }
  }
}
