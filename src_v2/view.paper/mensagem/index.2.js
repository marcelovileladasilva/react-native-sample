import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Caption, Paragraph, Title } from 'react-native-paper'
// import DropdownAlert from 'react-native-dropdownalert'
import { RequestResponse } from 'react-native-learning-paper'
import { TopHeaderPaper } from './../../component'

export default class TileMessage extends Component {
  constructor (props) {
    super(props)
    this.loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis
    auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque
    tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque
    pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis
    metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis
    dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh.
    Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
    this.state = { type: null }
  }

  render () {
    let message = {}
    message[ this.state.type ] = this.state.message
    return (
      <View flex-1 background-white>

        <TopHeaderPaper back navigator={this.props.navigator} />

        <ScrollView>
          <View padding-20>
            <View marginB-30>
              <Title>Mensagens de erro/sucesso</Title>
              <Caption>Exemplos de mensagens</Caption>
            </View>

            <View row spread marginB-10>
              <Paragraph>Erro em tela carregada</Paragraph>
              <Paragraph onPress={() => this.setState({ type: 'error', title: 'Erro', message: 'Mensagem de erro com tela carregada' })}>[Link]</Paragraph>
            </View>

            <View row spread marginB-10>
              <Paragraph>Erro fatal sem tela carregada</Paragraph>
              <Paragraph>[Link]</Paragraph>
            </View>

            <View row spread marginB-10>
              <Paragraph>Sucesso em tela carregada</Paragraph>
              <Paragraph onPress={() => this.setState({ type: 'success', title: 'Sucesso', message: 'Operação realizada com sucesso' })}>[Link]</Paragraph>
            </View>
          </View>
        </ScrollView>

        {this.state.type === 'success' &&
          <RequestResponse success={[ this.state.message ]} />
        }
        {this.state.type === 'error' &&
          <RequestResponse error={[ this.state.message ]} />
        }
      </View>
    )
  }
}
