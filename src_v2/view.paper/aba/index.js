import React, { Component } from 'react'
import { Picker, ScrollView, StyleSheet } from 'react-native'
import { View, TagsInput, TextInput, Text } from 'react-native-ui-lib'
import { ButtonGroup } from 'react-native-elements'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { Caption, Divider, Paragraph, Subheading, Title } from 'react-native-paper'
import { Colors, SimpleAccordion } from 'react-native-learning-paper'
import { TopHeaderPaper } from './../../component'

export default class TileTab extends Component {

  updateIndex(selectedIndex) { this.setState({ selectedIndex }) }

  state = {
    index: 0,
    routes: [
      { key: '1', title: 'Importante' },
      { key: '2', title: 'Antiga' },
      { key: '3', title: 'Normal' },
    ],
    verified: false
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar
    {...props}
    labelStyle={{ color: this.getStyle().color }}
    indicatorStyle={{ backgroundColor: this.getStyle().color }}
    style={{ backgroundColor: this.getStyle().backgroundColor }}
  />;

  _renderScene = SceneMap({
    '1': () => this.loremIpsum('Importante'),
    '2': () => this.loremIpsum('Antiga'),
    '3': () => this.loremIpsum('Normal'),
  });

  render() {
    return (
      <View flex-1 background-white>
        <TopHeaderPaper back title='Abas' subTitle='Exemplo de abas' />
        <TabViewAnimated
          style={styles.container}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderHeader={this._renderHeader}
          onIndexChange={this._handleIndexChange}
        />
      </View>
    )
  }

  loremIpsum(titulo = 'Lorem ipsum') {
    return (
      <ScrollView>
        <View padding-20>
          {a.map((val, key) =>
          <SimpleAccordion label={`${titulo} ${val}`} opened='true'>
            <Paragraph>{loremIpsum}</Paragraph>
          </SimpleAccordion>
          )}
        </View>
      </ScrollView>
    )
  }

  getStyle() {
    //const styleExpo = { backgroundColor: Colors[ 'mutedl' ], color: Colors[ 'primary' ] }
    const style = { backgroundColor: Colors[ 'primary' ], color: Colors[ 'white' ] }
    const styleExpo = { backgroundColor: Colors[ 'white' ], color: Colors[ 'muted' ] }
    return global.expo && Platform.OS === 'ios' ? styleExpo : style
  }
}

const a = [1, 2, 3]

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh. Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
