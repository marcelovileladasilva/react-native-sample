import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Caption, Divider, Subheading } from 'react-native-paper'
import { Colors } from 'react-native-learning-paper'

export default props => {
  let rows = props.results
  return (
    <View>
      <ScrollView>
        <View paddingB-0 marginT-30>

          {rows.map((Opcao, key) =>
            <View>
              <View paddingH-20>
                <Subheading>{Opcao.StTitulo}</Subheading>
                <Caption>{Opcao.StSubtitulo1} - {Opcao.StSubTitulo2}</Caption>
              </View>
              <Divider />
            </View>
          )}

        </View>
      </ScrollView>
    </View>
  )
}

const styles = {
  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}
