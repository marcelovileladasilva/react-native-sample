import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Icon } from 'react-native-elements'
import { Button, Caption, Card, Paragraph, Subheading, Title } from 'react-native-paper'
import { Colors } from 'react-native-learning-paper'
import { TopHeaderPaper } from './../../component'
import ModalCarregado from './carregado'

export default class TileModal extends Component {
  constructor (props) {
    super(props)
    this.state = { modalVisible: false, modalContent: '' }
  }

  render () {
    const sty = { box: { padding: 10 } }
    return (
      <View flex-1 background-white>

        <TopHeaderPaper back title='Modais' subTitle='Exemplo de modais' />

        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            <View marginB-30>
              <Title>Modais</Title>
              <Caption>Exemplos de modais</Caption>
            </View>

            {this.renderEstatico()}

            <Card style={sty.box}>
              <Subheading>Modais</Subheading>
              <Caption>Use os modais para exibir muita informação sobreposta na tela, sem precisar redirecionar.</Caption>

              <View row spread marginT-10>
                <Button primary raised onPress={() => { this.setState({ modalVisible: true, modalContent: 'estatico' }) }}>Estático</Button>
                <Button primary raised onPress={() => { this.setState({ modalVisible: true, modalContent: 'carregado' }) }}>Carregado</Button>
              </View>
            </Card>

          </View>
        </ScrollView>
      </View>
    )
  }

  renderEstatico () {
    return (
      <Modal
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
        transparent
      >
        <View centerV flex style={{ backgroundColor: '#FFFFFF50', marginTop: 50 }}>
          <View background-white marginH-20 style={styles.wellBox}>
            {this.state.modalContent === 'estatico' && this.renderModalEstatico()}
            {this.state.modalContent === 'carregado' && this.renderModalCarregado()}
          </View>
        </View>
      </Modal >
    )// https://stackoverflow.com/questions/36147082/react-native-style-opacity-for-parent-and-child
  }

  renderModalEstatico () {
    return (
      <View>
        <View row spread>
          <Subheading>Confirme o que deseja fazer</Subheading>
          <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
            <Icon name='close' size={24} color={'grey'} ref={this.onRef} />
          </TouchableOpacity>
        </View>
        <Paragraph>Tem certeza que deseja fazer isso?</Paragraph>

        <View right marginT-10>
          <Button primary
            onPress={() => { this.setState({ modalVisible: false }) }}>Salvar</Button>
        </View>
      </View>
    )
  }

  renderModalCarregado () {
    return (
      <ModalCarregado
        close={() => { this.setState({ modalVisible: false }) }}
        save={() => { this.setState({ modalVisible: false }) }}
      />
    )
  }
}

const styles = {
  wellBox: {
    borderColor: Colors[ 'dark30' ],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}
