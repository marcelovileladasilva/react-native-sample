import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, TextInput } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Button, Caption, Card, Paragraph, Subheading, Title } from 'react-native-paper'
import { Colors } from 'react-native-learning-paper'

export default class ModalCarregado extends Component {
  render () {
    return (
      <View>
        <View row spread>
          <Subheading>Modal com conteúdo carregado</Subheading>
          <TouchableOpacity onPress={() => { this.props.close() }}>
            <Icon name='close' size={24} color={'grey'} ref={this.onRef} />
          </TouchableOpacity>
        </View>
        <Caption>Tem certeza que deseja fazer isso?</Caption>

        <TextInput
          style={{ height: 40 }}
          placeholder='Email'
          onChangeText={(text) => { }}
                />

        <TextInput
          style={{ height: 40 }}
          placeholder='Segurança'
          onChangeText={(text) => { }}
                />

        <View right marginT-10>
          <Button primary
            onPress={() => { this.props.save() }}
                >Salvar</Button>
        </View>
      </View>
    )
  }
}

const styles = {
  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}
