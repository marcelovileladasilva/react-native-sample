import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, StyleSheet } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from 'react-native-learning-paper'
import { TopHeaderPaper } from './../../component'
import locations from './../../asset/static-data/locations'

const FilePickerManager = require('NativeModules').FilePickerManager
//import RNFetchBlob from 'react-native-fetch-blob'
import Upload from 'react-native-background-upload'

import MapView from 'react-native-maps'

import MapViewDirections from 'react-native-maps-directions';

const origin = {
  latitude: 42.642312,
  longitude: 18.111793
};
const destination = {
  latitude: 56.1858015,
  longitude: -4.0499006
};
const GOOGLE_MAPS_APIKEY = 'AIzaSyBNOJQsoYk1-_ba7jN6EliHpuCp1Nqz1bo';

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
})

//Rota(lib que automatiza, ja exibindo)
//https://github.com/bramus/react-native-maps-directions
module.exports = class TileHome extends React.Component {

  state = {
    title: 'winterfell',
    description: '',
    latitude: 56.1858015,
    longitude: -4.0499006,

    la: 56.1858015,
    lo: -4.0499006,
    ladel: 82.000,
    lodel: 44.0000
  };

  render() {
    const { region } = this.props
    console.log(region)

    return (
      <View flex-1 background-white>
        <TopHeaderPaper label='SAMPLE' navigator={this.props.navigator} />
        <ScrollView>
          <View height={250}>
            <View style={styles.container}>
              <MapView
                style={styles.map}
                region={{
                  latitude: this.state.latitude,
                  longitude: this.state.longitude,
                  //latitudeDelta: 1.000,
                  //longitudeDelta: 50.0000
                  latitudeDelta: 82.000,
                  longitudeDelta: 44.0000
                }}
              /*onRegionChange={(region) => {
                this.setState({
                  la: region.latitude,
                  lo: region.longitude,
                  ladel: region.latitudeDelta,
                  lodel: region.longitudeDelta
                })
              }}*/
              >
                {locations.map((Opcao, key) =>
                  <MapView.Marker
                    coordinate={{
                      latitude: Opcao.latitude,
                      longitude: Opcao.longitude
                    }}
                    title={Opcao.title}
                    description={"description"}
                  />
                )}
                <MapViewDirections
                  origin={origin}
                  destination={destination}
                  apikey={GOOGLE_MAPS_APIKEY}
                />
              </MapView>
            </View>
          </View>
          <View padding-20 paddingB-0 marginB-100>
            <View height={150} />
            <Text>{JSON.stringify(this.state)}</Text>
            <View>
              {locations.map((Opcao, key) =>
                <TouchableOpacity onPress={() => { this.setState({ ...Opcao }) }}>
                  <Text>{Opcao.title}</Text>
                  <Text>{Opcao.description}</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}
