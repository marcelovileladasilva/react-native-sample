import React from 'react'
import { ScrollView } from 'react-native'
import { View, Colors } from 'react-native-ui-lib'
import { Actions } from 'react-native-router-flux'
import { Button, Caption, Card, Divider, Paragraph, Subheading, Title } from 'react-native-paper'
import { SimpleAccordion } from 'react-native-learning-paper'
import { TopHeaderPaper } from './../../component'

export default props => {
  // import { BrandStyle as sty } from 'react-native-learning-paper' // Visual A
  const sty = {box: { padding: 10 /* ,borderLeftWidth: 5, borderColor: 'black' */ }}
  return (
    <View flex-1 background-white>
      <TopHeaderPaper back title='Caixas' subTitle='Exemplos de caixas' />

      <ScrollView>
        <View padding-20 marginB-50>

          <View marginB-30>
            <Title>Caixas</Title>
            <Caption>Exemplos de caixas de informação</Caption>
          </View>

          <Card style={sty.box}>
            <Paragraph>meusite.com</Paragraph>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <Caption>Meu site</Caption>
            <Paragraph>meusite.com</Paragraph>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <View row spread centerV>
              <Paragraph>meusite.com</Paragraph>
              <Button primary raised onPress={() => { }}>Administrar</Button>
            </View>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <View row spread centerV>
              <View>
                <Caption>Meu site</Caption>
                <Paragraph>meusite.com</Paragraph>
              </View>
              <Button primary raised onPress={() => { }}>Administrar</Button>
            </View>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <Paragraph>contato@meusite.com</Paragraph>
            <Paragraph>12,3GB de 40GB</Paragraph>
            <Paragraph>Criado em 10/07/2012</Paragraph>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <Subheading>Renovação dominio.com - 1 ano(s)</Subheading>
            <View row spread>
              <View>
                <Paragraph>R$ 49,90</Paragraph>
                <Caption>Valor</Caption>
              </View>
              <View>
                <Paragraph>email@meusite.com</Paragraph>
                <Caption>Email</Caption>
              </View>
              <View>
                <Paragraph>site123.net</Paragraph>
                <Caption>Cliente</Caption>
              </View>
            </View>
          </Card>
          <View marginB-10 />

          <Card style={sty.box}>
            <View row spread paddingB-10>
              <Caption>Meu site</Caption>
              <Paragraph>meusite.com</Paragraph>
            </View>
            <Divider />
            <View row spread paddingV-10 centerV>
              <View>
                <Caption>Meu site</Caption>
                <Paragraph>meusite.com</Paragraph>
              </View>
              <Button primary raised onPress={() => { }}>Administrar</Button>
            </View>
            <Divider />
            <View row spread paddingT-10>
              <View>
                <Paragraph>Meu site</Paragraph>
                <Paragraph>12,3GB de 40GB</Paragraph>
              </View>
              <Paragraph>Criado em 10/07/2012</Paragraph>
            </View>
          </Card>
          <View marginB-10 />

          <View marginB-30 />

          <View marginB-30>
            <Title>Caixas separadoras</Title>
            <Caption>Exemplos de caixas separadoras</Caption>
          </View>

          <SimpleAccordion label='Caixa 1' opened>
            <Paragraph>{loremIpsum}</Paragraph>
          </SimpleAccordion>
          <SimpleAccordion label='Caixa 2222' opened>
            <Paragraph>{loremIpsum}</Paragraph>
          </SimpleAccordion>

        </View>
      </ScrollView>

    </View>
  )
}

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque placerat dui vel enim viverra facilisis. Nam mollis auctor augue eget blandit.Vivamus vitae ipsum nisl.Phasellus vitae purus quis enim egestas volutpat.Proin scelerisque tristique libero vitae ullamcorper.Sed vel sem non mi auctor porta.Ut sagittis, mi iaculis feugiat posuere, neque neque pretium felis, eget consequat lectus libero at leo.Etiam vestibulum dictum nisl, ut feugiat est tincidunt vitae.Duis metus ligula, scelerisque id est mattis, placerat tempus magna.Phasellus ex urna, sodales in risus dapibus, lobortis dictum ipsum.Morbi posuere, ex quis tempus suscipit, risus metus mollis eros, ut fringilla tellus libero sit amet nibh. Duis fermentum bibendum egestas.In sit amet enim ut turpis lacinia hendrerit.`
