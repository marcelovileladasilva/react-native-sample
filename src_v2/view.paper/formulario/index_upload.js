import React, { Component } from 'react'
import { ScrollView, Modal, Platform } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Button, Caption, Card, Divider, Paragraph, Subheading, Title } from 'react-native-paper'
import { Colors } from 'react-native-learning-paper'
import { TopHeaderPaper, Transfer } from './../../component'

export default class TileHome extends Component {
  state = { file: undefined, response: undefined }

  async fileHandler() {
    t = await new Transfer()
    t.selectFileTapped(this.selectFileTappedCallBack.bind(this))
  }

  async selectFileTappedCallBack(r) {
    this.setState({file: r})
    if (!r.cancelled) {
      this.upload()
    }
  }

  async upload() {
    t = await new Transfer()
    const response = await t.upload(this.state.file)
    this.setState({ response })
  }

  render() {
    return (
      <View flex-1 background-white>
        <TopHeaderPaper back title='File Picker' subTitle='Exemplo de file picker' />
        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            <View center>
              <Button primary raised onPress={this.fileHandler.bind(this)}>Selecionar arquivo</Button>
            </View>

            <Subheading>Dados obtidos ao ler arquivo localmente:</Subheading>
            <Caption>{JSON.stringify(this.state.file)}</Caption>

            <Subheading>Resposta da requisicao:</Subheading>
            <Caption>{JSON.stringify(this.state.response)}</Caption>
          </View>

        </ScrollView>
      </View>
    )
  }
}
