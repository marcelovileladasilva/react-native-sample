import React, { Component } from 'react'
import { Button, Modal, Picker, ScrollView, StyleSheet } from 'react-native'
import { View, TagsInput, TextInput, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Caption, Card, Divider, Paragraph, Subheading, Title } from 'react-native-paper'
import _ from 'lodash'
import { Colors, FilterBar, PopupMenu, TopHeader, TreatList } from 'react-native-learning-paper'
import list from './../../asset/static-data/pessoas.2'

export default class TableC extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: '',
      page: 0,
      orderColumn: 'id',
      orderDirection: 'asc',
      selection: []
    }
    this.refreshActions = this.refreshActions.bind(this)
  }

  refreshActions (action) { this.setState(action) }

  render_ () {
    return (
      <View><Text>123</Text></View>
    )
  }

  render () {
    // import { BrandStyle as sty } from 'react-native-learning-paper' // Visual A
    const sty = { box: { padding: 10 /* ,borderLeftWidth: 5, borderColor: 'black' */ } }

    s = this.state
    t = new TreatList()
    columns = { 'StNome': 'Nome', 'StEmail': 'Email' }
    listTreated = t.getTreatedItens(list, s.text, Object.keys(columns), s.page, s.orderColumn, s.orderDirection)

    return (
      <View>
        <View marginB-30>
          <Title>Compartilhamento</Title>
        </View>

        <Card style={sty.box}>
          <View row spread>
            <Caption>Arquivo</Caption>
            <Subheading>/home/usuario/Smash.jpg</Subheading>
          </View>
          <Divider />
          <View row spread>
            <Caption>Dono</Caption>
            <Subheading>Eu</Subheading>
          </View>
        </Card>

        <View marginB-30 />

        <View marginB-30>
          <Title>Tabelas</Title>
          <Caption>Exemplos de tabelas</Caption>
        </View>

        <FilterBar refreshActions={this.refreshActions} actions={this.state} order={columns} />

        {listTreated.data.map((Item, key) =>
          <View key={key}>
            <View row flex>
              <View flex>
                <Subheading>{Item.StNome}</Subheading>
                <Paragraph>{Item.StEmail}</Paragraph>
              </View>
            </View>
          </View>
        )}

      </View>
    )
  }
}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  },

  cardBox: {
    borderColor: Colors[ 'dark30' ],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10,
    margin: 0
  }
}

/*
            <View>
              <Picker
                selectedValue={this.state.orderColumn}
                onValueChange={(orderColumn, itemIndex) => this.setState(
                  { orderColumn }
                )}>
                <Picker.Item label={'Nome'} value={'title'} />
                <Picker.Item label={'ID'} value={'id'} />
              </Picker>

              <Picker
                selectedValue={this.state.orderDirection}
                onValueChange={(orderDirection, itemIndex) => this.setState(
                  { orderDirection }
                )}>
                <Picker.Item label={'Crescente'} value={'asc'} />
                <Picker.Item label={'Decrescente'} value={'desc'} />
              </Picker>
            </View>
*/
