import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, StyleSheet } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from 'react-native-learning'
import { TopHeaderSample } from './../../component'
import locations from './../../asset/static-data/locations'

const FilePickerManager = require('NativeModules').FilePickerManager
//import RNFetchBlob from 'react-native-fetch-blob'
import Upload from 'react-native-background-upload'

import MapView from 'react-native-maps'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
})

module.exports = class TileHome extends React.Component {

  state = {
    latitude: 36.052371,
    longitude: 14.189879,
  };

  render() {
    const { region } = this.props
    console.log(region)

    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' navigator={this.props.navigator} />
        <ScrollView>
          <View height={250}>
            <View style={styles.container}>
              <MapView
                style={styles.map}
                region={{
                  latitude: this.state.latitude,
                  longitude: this.state.longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121
                }}
              />
            </View>
          </View>
          <View padding-20 paddingB-0 marginB-100>
          <View height={150} />
            {locations.map((Opcao, key) =>
              <TouchableOpacity onPress={() => { this.setState({ latitude: Opcao.latitude, longitude: Opcao.longitude }) }}>
                <Text>{Opcao.title}</Text>
                <Text>{Opcao.link}</Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
      </View>
    )
  }
}
