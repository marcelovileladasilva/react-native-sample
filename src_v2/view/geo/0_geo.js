
import React, { Component } from 'react'
import { View, Text, PermissionsAndroid, ScrollView } from 'react-native'
import { TreatMoment } from 'react-native-learning'
import { TopHeaderSample } from './../../component'

class GeolocationExample extends Component {
  constructor (props) {
    super(props)

    this.state = {
      latitude: null,
      longitude: null,
      error: null
    }
  }

  async componentDidMount () {
    // await perm()
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        // alert(position)
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        })
      },
      (error) => { /* alert(error); */; this.setState({ error: error.message }) },
      { timeout: 20000, maximumAge: 1000, distanceFilter: 10, enableHighAccuracy: false }
    )
  }

  componentWillUnmount () {
    navigator.geolocation.clearWatch(this.watchId)
  }

  render () {
    const m = new TreatMoment()

    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' navigator={this.props.navigator} />
        <ScrollView>

          <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>{m.getFormated('2018-01-05T07:19:22.176')}</Text>
            <Text>{m.getFormated('2018-01-01T07:19:22.176')}</Text>
            <Text>{m.getFormated('2017-01-01T07:19:22.176')}</Text>
            <Text>Latitude: {this.state.latitude}</Text>
            <Text>Longitude: {this.state.longitude}</Text>
            {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
          </View>

        </ScrollView>
      </View>
    )
  }
}

const perm = async function requestCameraPermission () {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Cool Photo App Camera Permission',
        'message': 'Cool Photo App needs access to your camera ' +
          'so you can take awesome pictures.'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera')
      alert(1)
      return 1
    } else {
      console.log('Camera permission denied')
      alert(2)
      return 2
    }
  } catch (err) {
    console.warn(err)
    alert(3)
    return 3
  }
}

export default GeolocationExample
