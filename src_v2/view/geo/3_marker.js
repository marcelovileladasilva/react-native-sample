import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, StyleSheet } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from 'react-native-learning'
import { TopHeaderSample } from './../../component'
import locations from './../../asset/static-data/locations'

const FilePickerManager = require('NativeModules').FilePickerManager
//import RNFetchBlob from 'react-native-fetch-blob'
import Upload from 'react-native-background-upload'

import MapView from 'react-native-maps'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
})

module.exports = class TileHome extends React.Component {

  state = {
    title: 'winterfell',
    description: '',
    latitude: 47.2786212,
    longitude: 12.4064045,
    latitudeDelta: 28.5136258,
    longitudeDelta: 43.5828240,
    x: {}
  };

  render() {
    const { region } = this.props
    console.log(region)

    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' navigator={this.props.navigator} />
        <ScrollView>
          <View height={250}>
            <View style={styles.container}>
              <MapView
                style={styles.map}
                initialRegion={{
                  latitude: this.state.latitude,
                  longitude: this.state.longitude,
                  latitudeDelta: this.state.latitudeDelta,
                  longitudeDelta: this.state.longitudeDelta,
                }}
                region={{
                  latitude: this.state.latitude,
                  longitude: this.state.longitude,
                  latitudeDelta: this.state.latitudeDelta,
                  longitudeDelta: this.state.longitudeDelta,
                }}
                onRegionChangeComplete={(e) => this.setState({ ...e })}
              >
                {locations.map((Opcao, key) =>
                  <MapView.Marker
                    coordinate={{
                      latitude: Opcao.latitude,
                      longitude: Opcao.longitude
                    }}
                    title={Opcao.title}
                    description={"description"}
                  //image={require('./../../img/placeholder.png')}
                  //image={{ uri: 'https://facebook.github.io/react/logo-og.png', width: 40, height: 40 }}
                  //image={{ uri: 'https://facebook.github.io/react/logo-og.png', "width": 40, "height": 40 }}
                  //image={{ uri: 'https://facebook.github.io/react/logo-og.png', style: { width: 40, height: 40 } }}
                  //image={{ uri: 'https://facebook.github.io/react/logo-og.png' }} width={40} height={40}
                  />
                )}
              </MapView>
            </View>
          </View>
          <View padding-20 paddingB-0 marginB-100>
            <View height={150} />
            <Text>{JSON.stringify(this.state)}</Text>
            {locations.map((Opcao, key) =>
              <TouchableOpacity onPress={() => { this.setState({ ...Opcao }) }}>
                <Text>{Opcao.title}</Text>
                {/*<Text>{Opcao.description}</Text>*/}
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
      </View >
    )
  }
}
