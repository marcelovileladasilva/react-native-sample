import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
import { Colors } from 'react-native-learning'

export default props => {
  let rows = props.results
  return (
    <View>
      <ScrollView>
        <View padding-20 paddingB-0 marginB-50>

          {rows.map((Opcao, key) =>
            <View marginB-10 style={styles.wellBox}>
              <Text>{Opcao.StTitulo}</Text>
              <Text>{Opcao.StSubtitulo1} - {Opcao.StSubTitulo2}</Text>
            </View>
                    )}

        </View>
      </ScrollView>
    </View>
  )
}

const styles = {
  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}
