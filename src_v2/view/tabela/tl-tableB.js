import React, { Component } from 'react';
import { Button, Modal, Picker, ScrollView, StyleSheet } from 'react-native';
import { View, TagsInput, TextInput, Text } from 'react-native-ui-lib';
import { Icon, ButtonGroup } from 'react-native-elements'
import _ from 'lodash'
import { Colors, FilterBar, PopupMenu, TopHeader, TreatList } from 'react-native-learning'
import list from './../../asset/static-data/pessoas.1'

export default class TableB extends Component {

  constructor(props) {
    super(props)
    this.state = {
      text: '', page: 0, orderColumn: 'id', orderDirection: 'asc',
      selection: []
    }
    this.refreshActions = this.refreshActions.bind(this)
  }

  refreshActions(action) { this.setState(action) }

  opcao1() { alert('Adicione comportamento da Opção 1 aqui...') }
  opcao2() { alert('Adicione comportamento da Opção 2 aqui...') }
  //<Button title="Opção A" onPress={() => { this.opcao1() }} />
  //<View marginB-10 />
  //<Button title="Opção B" onPress={() => { this.opcao2() }} />

  render() {
    s = this.state
    t = new TreatList()
    columns = {
      'StUsuario': 'Usuário', 'StAcao': 'Ação', 'StObjeto': 'Objeto',
      'StDispositivo': 'Dispositivo', 'StDescricao': 'Descrição', 'StData': 'Data'
    }
    listTreated = t.getTreatedItens(list, s.text, Object.keys(columns), s.page, s.orderColumn, s.orderDirection)

    return (
      <View>

        <View marginB-30>
          <Text h1>Tabelas</Text>
          <Text>Exemplos de tabelas</Text>
        </View>

        <FilterBar refreshActions={this.refreshActions} actions={this.state} order={columns}>
          <PopupMenu actions={['Opção 1', 'Opção 2']} onPress={this.onPopupOptions} />
        </FilterBar>

        {listTreated.data.map((Item, key) =>
          <View marginB-10 style={styles.wellBox} key={key}>
            <View row spread>
              <Text>{Item.StUsuario} - {Item.BoChecked}</Text>
              <PopupMenu actions={['Ação 1', 'Ação 2', 'Ação 3']} onPress={this.onPopupEvent} />
            </View>
            <Text>{Item.StAcao}</Text>
            <Text>{Item.StObjeto}</Text>
            <Text>{Item.StDispositivo}</Text>
            <Text>{Item.StDescricao}</Text>
            <Text>{Item.StData}</Text>
          </View>
        )}

      </View>
    )
  }

  onPopupEvent = (eventName, index) => {
    //alert(`${eventName} - ${index}`)
    if (eventName !== 'itemSelected') return
    if (index === 0) alert('Comportamento da ação 1')
    if (index === 1) alert('Comportamento da ação 2')
    if (index === 2) alert('Comportamento da ação 3')
  }

  onPopupOptions = (eventName, index) => {
    //alert(`${eventName} - ${index}`)
    if (eventName !== 'itemSelected') return
    if (index === 0) alert('Adicione comportamento da Opção 1 aqui...')
    if (index === 1) alert('Adicione comportamento da Opção 2 aqui...')
  }

}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}

/*
            <View>
              <Picker
                selectedValue={this.state.orderColumn}
                onValueChange={(orderColumn, itemIndex) => this.setState(
                  { orderColumn }
                )}>
                <Picker.Item label={'Nome'} value={'title'} />
                <Picker.Item label={'ID'} value={'id'} />
              </Picker>

              <Picker
                selectedValue={this.state.orderDirection}
                onValueChange={(orderDirection, itemIndex) => this.setState(
                  { orderDirection }
                )}>
                <Picker.Item label={'Crescente'} value={'asc'} />
                <Picker.Item label={'Decrescente'} value={'desc'} />
              </Picker>
            </View>
*/
