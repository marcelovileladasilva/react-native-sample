import React, { Component } from 'react';
import { Modal, Picker, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { View, TagsInput, TextInput, Text, Colors, Button } from 'react-native-ui-lib';
import { Icon, ButtonGroup } from 'react-native-elements'
import _ from 'lodash'
import TreatList from './../../../df-dataview/TreatList';
import FilterBar from './../../../df-dataview/FilterBar';
import PopupMenu from './../../../df-popup/PopupMenu';
import list from './../../asset/static-data/pessoas'

// Trata selecao multipla armazenando valor do item (ex: ID pertencente ao objeto) no array state.selection
export default class TableA extends Component {

  constructor(props) {
    super(props)
    this.state = {
      text: '', page: 0, orderColumn: 'id', orderDirection: 'asc',
      selection: [], enableMultiple: false
    }
    this.refreshActions = this.refreshActions.bind(this)
  }

  refreshActions(action) { this.setState(action) }

  render() {
    s = this.state
    //alert(s.selection)
    t = new TreatList()
    columns = {
      'StUsuario': 'Usuário', 'StAcao': 'Ação', 'StObjeto': 'Objeto',
      'StDispositivo': 'Dispositivo', 'StDescricao': 'Descrição', 'StData': 'Data'
    }
    listTreated = t.getTreatedItens(list, s.text, Object.keys(columns), s.page, s.orderColumn, s.orderDirection)

    return (
      <View>

        <View marginB-30>
          <Text text60 blue30>Tabelas {s.enableMultiple ? 'true' : 'false'}</Text>
          <Text>Exemplos de tabelas - </Text>
        </View>

        <FilterBar refreshActions={this.refreshActions} actions={this.state} order={columns} />

        <PopupMenu actions={[ 'Seleção múltipla' ]} onPress={this.onPopupMultEvent} />

        <Button size='small' background-red30 label="Remover" onPress={() => { }} />
        <Text>Selecionados{_.toString(this.state.selection)}</Text>

        {listTreated.data.map((Item, key) =>
          <View marginB-10 style={styles.wellBox}>
            <View row spread>
              <Text>{Item.StUsuario} - {Item.BoChecked}</Text>
              {!s.enableMultiple &&
                <PopupMenu actions={[ 'Ação 1', 'Ação 2', 'Ação 3' ]} onPress={this.onPopupEvent} />
              }
              {s.enableMultiple && s.selection.indexOf(Item.StUsuario) < 0 &&
                <TouchableOpacity onPress={() => {
                  s.selection.push(Item.StUsuario);
                  this.setState({ selection: s.selection })
                }}>
                  <Icon name='check-box-outline-blank' size={24} color={'grey'} ref={this.onRef} />
                </TouchableOpacity>
              }
              {s.enableMultiple && s.selection.indexOf(Item.StUsuario) > -1 &&
                <TouchableOpacity onPress={() => {
                  s.selection.splice(s.selection.indexOf(Item.StUsuario), 1);
                  this.setState({ selection: s.selection })
                }}>
                  <Icon name='check-box' size={24} color={'grey'} ref={this.onRef} />
                </TouchableOpacity>
              }
            </View>
            <Text>{Item.StAcao}</Text>
            <Text>{Item.StObjeto}</Text>
            <Text>{Item.StDispositivo}</Text>
            <Text>{Item.StDescricao}</Text>
            <Text>{Item.StData}</Text>
          </View>
        )}

      </View>
    )
  }

  onPopupEvent = (eventName, index) => {
    //alert(`${eventName} - ${index}`)
    if (eventName !== 'itemSelected') return
    if (index === 0) alert('Comportamento da ação 1')
    if (index === 1) alert('Comportamento da ação 2')
    if (index === 2) alert('Comportamento da ação 3')
  }

  onPopupMultEvent = (eventName, index) => {
    this.setState({ enableMultiple: !this.state.enableMultiple })
  }

}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  wellBox: {
    borderColor: Colors[ 'dark30' ],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}

/*
        <View>
          <Picker
            selectedValue={this.state.orderColumn}
            onValueChange={(orderColumn, itemIndex) => this.setState(
              { orderColumn }
            )}>
            <Picker.Item label={'Nome'} value={'title'} />
            <Picker.Item label={'ID'} value={'id'} />
          </Picker>

          <Picker
            selectedValue={this.state.orderDirection}
            onValueChange={(orderDirection, itemIndex) => this.setState(
              { orderDirection }
            )}>
            <Picker.Item label={'Crescente'} value={'asc'} />
            <Picker.Item label={'Decrescente'} value={'desc'} />
          </Picker>
        </View>
        */
