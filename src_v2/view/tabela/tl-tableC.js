import React, { Component } from 'react'
import { Button, Modal, Picker, ScrollView, StyleSheet } from 'react-native'
import { View, TagsInput, TextInput, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup, Divider } from 'react-native-elements'
import _ from 'lodash'
import { Colors, FilterBar, PopupMenu, TopHeader, TreatList } from 'react-native-learning'
import list from './../../asset/static-data/pessoas.2'

export default class TableC extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: '',
      page: 0,
      orderColumn: 'id',
      orderDirection: 'asc',
      selection: []
    }
    this.refreshActions = this.refreshActions.bind(this)
  }

  refreshActions (action) { this.setState(action) }

  render_ () {
    return (
      <View><Text>123</Text></View>
    )
  }

  render () {
    s = this.state
    t = new TreatList()
    columns = { 'StNome': 'Nome', 'StEmail': 'Email' }
    listTreated = t.getTreatedItens(list, s.text, Object.keys(columns), s.page, s.orderColumn, s.orderDirection)

    return (
      <View>
        <View marginB-30>
          <Text h1>Compartilhamento</Text>
        </View>

        <View marginB-10 style={styles.wellBox}>
          <View row spread paddingB-10>
            <Text>Arquivo</Text>
            <Text>/home/usuario/Smash.jpg</Text>
          </View>
          <Divider />
          <View row spread paddingV-10>
            <Text>Dono</Text>
            <Text>Eu</Text>
          </View>
        </View>

        <View marginB-30 />

        <View marginB-30>
          <Text h1>Tabelas {this.state.text}</Text>
          <Text>Exemplos de tabelas</Text>
        </View>

        <FilterBar refreshActions={this.refreshActions} actions={this.state} order={columns} />

        {listTreated.data.map((Item, key) =>
          <View marginB-10 style={styles.wellBox} key={key}>
            <View row spread>
              <Text>{Item.StNome} - {Item.BoChecked}</Text>
            </View>
            <Text>{Item.StEmail}</Text>
          </View>
        )}

      </View>
    )
  }
}

const styles = {
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  wellBox: {
    borderColor: Colors['dark30'],
    borderWidth: 1,
    borderLeftWidth: 5,
    padding: 10
  }
}

/*
            <View>
              <Picker
                selectedValue={this.state.orderColumn}
                onValueChange={(orderColumn, itemIndex) => this.setState(
                  { orderColumn }
                )}>
                <Picker.Item label={'Nome'} value={'title'} />
                <Picker.Item label={'ID'} value={'id'} />
              </Picker>

              <Picker
                selectedValue={this.state.orderDirection}
                onValueChange={(orderDirection, itemIndex) => this.setState(
                  { orderDirection }
                )}>
                <Picker.Item label={'Crescente'} value={'asc'} />
                <Picker.Item label={'Decrescente'} value={'desc'} />
              </Picker>
            </View>
*/
