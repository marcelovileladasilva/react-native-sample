import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { View, Text } from 'react-native-ui-lib'
// import TopHeader from './../../../df-layout/TopHeader'
import { Colors } from 'react-native-learning'
import { TopHeaderSample } from './../../component'
import TableA from './tl-tableA'
import TableB from './tl-tableB'
import TableC from './tl-tableC'

export default class TileTable extends Component {
  render () {
    return (
      <View flex-1 background-white>

        <TopHeaderSample label='SAMPLE' back navigator={this.props.navigator} />

        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>
            <TableA />
            <View marginB-30 />
            <TableB />
            <View marginB-30 />
            <TableC />
          </View>
        </ScrollView>
      </View>
    )
  }
}
