import React, { Component } from 'react'
import { ScrollView, Modal, Platform } from 'react-native'
import { Button, View, Text } from 'react-native-ui-lib'
import { Icon, ButtonGroup } from 'react-native-elements'
import { Colors } from 'react-native-learning'
import { TopHeaderSample } from './../shared'

//import { ImagePicker, DocumentPicker } from 'expo';
const FilePickerManager = require('NativeModules').FilePickerManager

//import RNFetchBlob from 'react-native-fetch-blob'
import Upload from 'react-native-background-upload'

export default class TileHome extends Component {
  state = { file: undefined, response: undefined }

  // Seleciona arquivo
  selectFileTapped(fn) {
    let response = { cancelled: true }

    // Create app > Android
    if (!global.expo && Platform.OS === 'android') {
      FilePickerManager.showFilePicker({}, async function r(r) {
        if (!r.didCancel && !r.error && !r.customButton) {
          response = r
          response.cancelled = false
        }
        fn(response)
      })
    }

    // Expo > Android/IOS
    else {
      let r = DocumentPicker.getDocumentAsync({})
      if (!r.cancelled) {
        r.path = r.uri
      }
      response = r
      fn(response)
    }
  }

  async fileHandler() {
    this.selectFileTapped(this.selectFileTappedCallBack.bind(this))
  }

  async selectFileTappedCallBack(r) {
    this.setState({ file: r })
    if (!r.cancelled) {
      this.up()
    }
  }

  // Envia arquivo
  async up() {
    if (!global.expo) {
      let opt = { ...options }
      opt.path = this.state.file.path

      Upload.startUpload(opt).then((uploadId) => {
        console.log('Upload started')
        Upload.addListener('progress', uploadId, (data) => {
          console.log(`Progress: ${data.progress}%`)
        })
        Upload.addListener('error', uploadId, (data) => {
          alert(`Error: ${data.error}%`)
        })
        Upload.addListener('cancelled', uploadId, (data) => {
          console.log(`Cancelled!`)
        })
        Upload.addListener('completed', uploadId, (data) => {
          this.setState({ response: data });
          console.log('Completed!')
        })
      }).catch((err) => {
        console.log('Upload error!', err)
      })
    } else {
      let localUri = this.state.file.uri;
      let filename = this.state.file.name

      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[ 1 ]}` : `application/${this.state.file.name.split('.').pop()}`;

      let formData = new FormData();
      formData.append('photo', { uri: localUri, name: filename, type });

      //alert(JSON.stringify(formData))
      //return false

      const response = await fetch('http://aplicativ0s.tmp.k8.com.br/teste.php?a=enviadopelonative13', {
        method: 'POST',
        body: formData,
        header: {
          'content-type': 'multipart/form-data',
        },
      }).then(function (response) { return response.json() })
      this.setState({ response })
    }
  }

  render() {
    return (
      <View flex-1 background-white>
        <TopHeaderSample back navigator={this.props.navigator} />
        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            <Button size='small' background-primary label='Selecionar arquivo' onPress={this.fileHandler.bind(this)} />

            <Text>Dados obtidos ao ler arquivo localmente:</Text>
            <Text>{JSON.stringify(this.state.file)}</Text>

            <Text>Resposta da requisicao:</Text>
            <Text>{JSON.stringify(this.state.response)}</Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}

var options = {
  url: 'http://aplicativ0s.tmp.k8.com.br/teste.php?a=enviadopelonative13',
  path: 'file://path/to/file/on/device',
  method: 'POST',
  field: 'uploaded_media',
  type: 'multipart',
  headers: {
    'my-custom-header': 's3headervalueorwhateveryouneed'
  },
  // Below are options only supported on Android
  notification: {
    enabled: true
  }
}
