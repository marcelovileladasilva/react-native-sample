import React, { Component } from 'react'
import { ScrollView, Modal, TouchableOpacity, StyleSheet, TextInput, Picker } from 'react-native'
import { ButtonGroup, Slider, Icon } from 'react-native-elements'
import { Button, Text, View } from 'react-native-ui-lib'
import { Colors, BrandStyle as Sty, BrandStyle, StrengthView, Validation, Mask } from 'react-native-learning'
import { FormBox, FormLabel, FormTextboxView } from 'react-native-learning'
import { TopHeaderSample, Transfer } from './../../component'
import moment from 'moment'
import _ from 'lodash'
import t from 'tcomb-form-native'

// V6 - Ajuste de estilos - compara a sugestao da documentacao OU ao visual atual
export default class TileHome extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isValid: null,
      value: {
        maisOpcoes: 1,
        escolhaÚnica: 0,
        senha: 'MinhaSenha123',
        confirmarSenha: 'MinhaSenha123',
        sliderNumerico: 100,
        celularDDI: '123',
        celularDDD: '12',
        celularNúmero: '91234-5678',
        TelefoneDDI: '123',
        TelefoneDDD: '12',
        TelefoneNúmero: '1234-5678',
        nome: 'Eudes',
        email: 'eudes@hostnet.com.br',
        confirmarEmail: 'eudes@hostnet.com.br',
        emailSecundário: 'eudes2@hostnet.com.br',
        switchButtonComDica: false,
        switchButton: true,
        escolhaMúltipla: []
      },
      file: {}
    }
    this.formStyle = BrandStyle.stylesheet
    // Bind (Ajuste para chamar metodos desta classe no contexto de outros objetos)
    this.mySliderTemplate = this.mySliderTemplate.bind(this)
    this.myPasswordTemplate = this.myPasswordTemplate.bind(this)
    this.mySwitchTemplate = this.mySwitchTemplate.bind(this)
    this.renderMultiple = this.renderMultiple.bind(this)
    this.renderFile = this.renderFile.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  render () {
    const s = this.state
    let Form = t.form.Form

    // Estruturacao do form: Associar campo 'a validacao
    const v = new Validation()

    const tr = t.refinement
    let Person = t.struct({
      email: tr(t.String, v.email),
      confirmarEmail: tr(t.String, n => v.equal(n, this.state.value.email)),
      nome: t.String,
      TelefoneDDI: tr(t.Number, v.ddi),
      TelefoneDDD: tr(t.Number, v.ddd),
      TelefoneNúmero: tr(t.String, n => { return v.regexTest(n, 'telephone') }),
      celularDDI: tr(t.Number, v.ddi),
      celularDDD: tr(t.Number, v.ddd),
      celularNúmero: tr(t.String, n => { return v.regexTest(n, 'cellphone') }),
      data: t.maybe(t.Date),
      sliderNumerico: t.Number,
      senha: tr(t.String, n => { return v.password_strength(this.state.value.senha, false) }),
      confirmarSenha: tr(t.String, n => v.equal(n, this.state.value.senha)),
      emailSecundário: tr(t.String, v.email),
      maisOpcoes: t.Number,
      switchButtonComDica: t.Boolean,
      switchButton: t.Boolean,
      escolhaÚnica: t.enums(myList),
      escolhaMúltipla: t.Array,
      arquivo: t.maybe(t.String)
    })

    // Opcoes de renderizacao: Definir textos; configurar exibicao, associar 'a template especifico
    let options = {
      fields: {
        'email': { 'error': 'Informe um email válido' },
        'confirmarEmail': { 'error': 'Esses campos não conferem' },
        'nome': { 'error': 'Você precisa preencher este campo' },
        'TelefoneDDI': { 'error': 'Você precisa informar um número' },
        'TelefoneDDD': { 'error': 'Você precisa informar um número' },
        'TelefoneNúmero': { 'error': 'Você precisa informar um número', keyboardType: 'phone-pad' },
        'celularDDI': { 'error': 'Você precisa informar um número' },
        'celularDDD': { 'error': 'Você precisa informar um número' },
        'celularNúmero': { 'error': 'Você precisa informar um número', keyboardType: 'phone-pad' },
        'data': effectiveDate,
        'sliderNumerico': { template: this.mySliderTemplate },
        'senha': { template: this.myPasswordTemplate },
        'confirmarSenha': { 'error': 'Esses campos não conferem' },
        'emailSecundário': {
          'error': 'Informe um email válido',
          'help': 'O email de recuperação pode ser necessário para recuperar o acesso a sua conta'
        },
        'maisOpcoes': { template: this.mySwitchTemplate },
        'switchButtonComDica': { help: 'Aqui vai a dica para o switch button' },
        'escolhaÚnica': { label: 'Escolha única', error: 'Escolha uma opção' },
        'escolhaMúltipla': { label: 'Escolha múltipla', template: this.renderMultiple },
        'arquivo': { template: this.renderFile }
      },
      stylesheet: this.formStyle
    }
    const bgColor = s.isValid ? Colors[ 'primary' ] : Colors[ 'muted' ]
    return (
      <View flex-1 background-white>
        <TopHeaderSample label='SAMPLE' />
        <ScrollView>
          <View padding-20 paddingB-0 marginB-100>

            {this.myStaticTemplate()}
            <Form ref='form' type={Person} options={options}
              value={this.state.value} onChange={this.onChange}
            />
            <View row spread>
              <Button label='Salvar' size='small' background-primary />
              <Button label='Desabilitado' size='small' style={{ backgroundColor: bgColor }} />
              <Button label='Perigoso' size='small' background-danger />
            </View>
            <Text>{JSON.stringify(this.state.isValid)}</Text>

          </View>
        </ScrollView>
      </View>
    )
  }

  // Tratar mudanca de valores: Atualizar state e sinalizar se form eh  valido
  async onChange (value, extendValue = {}) {
    const m = new Mask()
    value[ 'TelefoneNúmero' ] = await m.alias(value[ 'TelefoneNúmero' ], 'telephone')
    value[ 'celularNúmero' ] = await m.alias(value[ 'celularNúmero' ], 'cellphone')
    this.setState({ value: Object.assign(value, extendValue) })
    this.setState({ isValid: this.refs.form.getValue() })
  }

  myStaticTemplate () {
    return (
      <View>
        <FormBox>
          <FormLabel label='Informação fixa' />
          <Text style={{ ...this.formStyle.helpBlock.normal }}>Hello, World!</Text>
        </FormBox>

        <FormBox>
          <FormLabel label='Informação fixa com botões' />
          <Text style={{ ...this.formStyle.helpBlock.normal }}>19,55GB (usado 0,45GB de 20GB)</Text>
          <View row>
            <Button background-primary size='small' label='Contratar mais' />
            <View width={10} />
            <Button background-primary size='small' label='Alterar plano' />
          </View>
        </FormBox>
      </View>
    )
  }

  // Renderizar especificamente
  mySwitchTemplate (locals) {
    const buttons = [ 'App e email', 'App', 'Nenhuma' ]
    return (
      <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
        <Text style={this.formStyle.controlLabel.normal}>Switch com 3 ou mais opções</Text>
        <ButtonGroup buttons={buttons} selectedIndex={this.state.value.maisOpcoes}
          onPress={maisOpcoes => { this.onChange(this.state.value, { maisOpcoes }) }}
        />
      </View>
    )
  }

  mySliderTemplate (locals) {
    return (
      <FormBox>
        <FormLabel label='Slider de número' />
        <View centerH>
          <Text>{this.state.value.sliderNumerico}</Text>
        </View>
        <Slider
          value={this.state.value.sliderNumerico}
          minimumValue={0} maximumValue={500} step={50} {...BrandStyle.slider}
          onValueChange={(sliderNumerico) => this.onChange(this.state.value, { sliderNumerico })}
        />
      </FormBox>
    )
  }

  myPasswordTemplate (locals) {
    const v = new Validation()
    const status = v.password_strength(this.state.value.senha, true)
    const stat = status.iStrengthScore > 2 ? 'normal' : 'error'

    return (
      <FormBox>
        <FormLabel label='Senha' />
        <FormTextboxView>
          <TextInput value={this.state.value.senha} style={this.formStyle.textbox[ stat ]}
            onChangeText={senha => { this.onChange(this.state.value, { senha }) }}
          />
        </FormTextboxView>
        <StrengthView score={status.iStrengthScore} percent={status.iStrengthPercent} />
      </FormBox>
    )
  }

  renderMultiple () {
    return (
      <FormBox>
        <FormLabel label='Escolha múltipla' />
        <View>
          <Picker
            selectedValue={null}
            onValueChange={(itemValue, itemIndex) => {
              this.onRemoveMultiple(itemValue)
              this.state.value.escolhaMúltipla.push(itemValue)
              this.onChange(this.state.value)
            }
            }>
            {Object.keys(myList).map((Item, key) =>
              <Picker.Item label={myList[ Item ]} value={Item} key={key} />
            )}
          </Picker>
          <View>
            {this.state.value.escolhaMúltipla.map((Item, key) =>
              <View marginB-10 row spread>
                <Text>{myList[ Item ]}</Text>
                <Icon name='remove' color={Colors[ 'red30' ]} size={22} onPress={() => this.onRemoveMultiple(Item)} />
              </View>
            )}
          </View>
        </View>
      </FormBox>
    )
  }

  async fileHandler () {
    const t = await new Transfer()
    t.selectFileTapped(this.selectFileTappedCallBack.bind(this))
  }

  async selectFileTappedCallBack (r) {
    // const file = {}; file[r.fileName] = r; this.setState({ file })
    this.onChange(this.state.value, { arquivo: r.fileName })
  }

  renderFile () {
    return (
      <FormBox>
        <View row spread>
          <FormLabel label='Selecionar um arquivo' />
          <Icon name='add' size={22} onPress={this.fileHandler.bind(this)} />
        </View>
        <View marginV-10>
          {this.state.value.arquivo &&  // Object.keys(this.state.file).map((Item, key) =>
            <View marginB-10 row spread>
              <Text>{this.state.value.arquivo}</Text>
              <Icon name='remove' color={Colors[ 'red30' ]} size={22} onPress={() => { this.onChange(this.state.value, { arquivo: null }) }} />
            </View>
          }
        </View>
      </FormBox>
    )
  }

  onRemoveMultiple (Item) {
    _.pull(this.state.value.escolhaMúltipla, Item)
    this.setState({ value: this.state.value })
  }
}

// NOTA 1 -> Validacoes executadas sequencialmente, ou seja soh chega na segunda apos a primeira estar OK
// NOTA 2 -> How to get access to a field -> //this.refs.form.getComponent('name').refs.input -> Propriedades descobertas atraves de Object.keys()
// Data (Solucao utilizada) => https://github.com/gcanti/tcomb-form-native/issues/189
// Data (Comentarios do criador) => https://github.com/gcanti/tcomb-form-native/issues/67
// Multiselect => https://github.com/gcanti/tcomb-form/issues/37

const myList = {
  0: 'Primeira opção com um textão super grande',
  1: 'Segunda opção com um texto médio',
  2: 'Terceira opção menor'
}

const myFormatFunction = (format, date) => {
  return moment(date).format(format)
}

const effectiveDate = {
  label: 'Data',
  mode: 'date',
  config: {
    format: (date) => myFormatFunction('DD/MM/YYYY', date)
  }
}
