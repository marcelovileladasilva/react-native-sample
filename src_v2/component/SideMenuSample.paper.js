import React, { Component } from 'react'
import { ScrollView, TouchableOpacity, AsyncStorage, Platform, StatusBar } from 'react-native'
import { Text, View } from 'react-native-ui-lib'
import { TopHeader, NavParent, PopupMenu } from 'react-native-learning-paper'
import { Actions } from 'react-native-router-flux'
import {
  Colors,
  DrawerItem,
  DrawerSection,
  withTheme,
  Switch,
  TouchableRipple,
  Paragraph,
  Subheading
} from 'react-native-paper'

let HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight

export default class SideMenuSample extends Component {
  constructor (props) {
    super(props)
    this.state = { sideMenuVisible: false }
    this.onPopupEvent = this.onPopupEvent.bind(this)
    this.toogleMenu = this.toogleMenu.bind(this)
  }

  toogleMenu (BoShow) {
    this.setState({ sideMenuVisible: BoShow })
  }

  render () {
    const StEmail = this.state.profileLoaded ? this.profile.StEmail : ''
    // GREEN50
    const n = { navigator: this.props.navigator }
    return (
      <View flex-1 background-white>
        <ScrollView>

          <View style={{height: HEIGHT}} />

          <View row spread centerV paddingT-20 paddingL-20>
            <Paragraph>padrão@hostnet.com.br</Paragraph>
            <PopupMenu actions={[ 'Conta', 'Ajuda', 'Sair' ]} onPress={this.onPopupEvent} />
          </View>

          <View marginB-10 />

          <NavParent label='Atendimento'>
            <DrawerItem label='Central de atendimento' icon='label-outline' />
            <DrawerItem label='Wiki Hostnet' icon='label-outline' />
            <DrawerItem label='2ª via boleto' icon='label-outline' />
          </NavParent>

          <NavParent label='Sistemas'>
            <DrawerItem label='Hostnet' icon='label-outline' />
            <DrawerItem label='Ultramail' icon='label-outline' />
            <DrawerItem label='Painel' icon='label-outline' />
            <DrawerItem label='Apps' icon='label-outline' />
            <DrawerItem label='Webfácil' icon='label-outline' />
            <DrawerItem label='Drive' icon='label-outline' />
          </NavParent>

          <DrawerSection>{/* title='Subheader' */}
            {this.renderItem('Formulário', 'inicial')}
            {this.renderItem('Tabela', 'tabela')}
            {this.renderItem('Caixas', 'caixa')}
            {this.renderItem('Abas', 'aba')}
            {this.renderItem('Erros', 'mensagem')}

            <NavParent label='Opções'>
              {this.renderItem('Modais', 'modal')}
              {this.renderItem('Menu flutuante')}
              {this.renderItem('Carregamento')}

              <NavParent label='Mais opções'>
                {this.renderItem('Outro link direto')}
                {this.renderItem('Mais opções')}
                {this.renderItem('File Picker', 'filepicker')}
                {global.expo &&
                  <View>
                    {this.renderItem('Estático', 'mapstatic')}
                    {this.renderItem('Estado', 'mapstate')}
                    {this.renderItem('Marcador', 'mapmarker')}
                    {this.renderItem('Rota - Directions', 'mapdirections')}
                    {this.renderItem('Rota - Polyline', 'mappolyline')}
                    {this.renderItem('Geolocation', 'geolocation')}
                    {this.renderItem('Geo + mapa', 'geomap')}
                  </View>
                }
              </NavParent>

              {this.renderItem('Link direto')}
              {this.renderItem('Link direto')}
            </NavParent>
          </DrawerSection>
        </ScrollView>
      </View>
    )
  }

  renderItem (label, route) {
    return this.renderNavChild({ label, route, icon: 'label-outline' }, null)
  }

  renderNavChild (props, index) {
    return (
      <DrawerItem
        {...props}
        key={props.key}
        color={props.key === 3 ? Colors.tealA200 : undefined}
        active={this.state.drawerItemIndex === index}
        onPress={() => {
          if (props.route) {
            Actions[ props.route ]()
          }
        }}
      />
    )
  }

  onPopupEvent (eventName, index) {
    if (eventName !== 'itemSelected') return
    if (index === 0) alert('Comportamento - Conta')
    if (index === 1) alert('Comportamento - Ajuda')
    if (index === 2) alert('Comportamento - Sair')
  }
}

/*
const DrawerItemsData = [
  { label: 'Formulário', icon: 'label-outline', key: 0, route: 'inicial' },
  { label: 'Tabelas', icon: 'label-outline', key: 2, route: 'tabela' },
  { label: 'Caixas', icon: 'label-outline', key: 3, route: 'caixa' },
  { label: 'Abas', icon: 'label-outline', key: 5, route: 'aba' },
  { label: 'Erros', icon: 'label-outline', key: 6, route: 'mensagem' },
  { label: 'Opções', icon: 'label-outline', key: 4, route: 'modal' }
]

  < View >
  <DrawerSection title='Subheader'>
    {DrawerItemsData.map((props, index) => (
      <DrawerItem
        {...props}
        key={props.key}
        color={props.key === 3 ? Colors.tealA200 : undefined}
        active={this.state.drawerItemIndex === index}
        // onPress={() => this._setDrawerItem(index)}
        onPress={() => Actions[ props.route ]()}
      />
    ))}
  </DrawerSection>
          </View >
*/
