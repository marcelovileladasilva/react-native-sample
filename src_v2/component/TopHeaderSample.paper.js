import React from 'react'
import { TopHeader } from 'react-native-learning-paper'
import { Actions } from 'react-native-router-flux'

export const TopHeaderPaper = props => {
  const label = ('label' in props) ? { title: props.label } : { title: 'SAMPLE' }
  return (
    <TopHeader
      {...label}
      {...props}
      toogleMenu={() => { Actions.drawerOpen() }}
      onBack={() => { Actions.pop() }}
      onSearch={() => { Actions.busca() }}
    />
  )
}
