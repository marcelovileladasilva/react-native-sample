const FilePickerManager = require('NativeModules').FilePickerManager
export class CreateAndroidPicker {
  selectFileTapped (fn) {
    let response = { cancelled: true }

    FilePickerManager.showFilePicker({}, async function r (r) {
      if (!r.didCancel && !r.error && !r.customButton) {
        response = r
        response.cancelled = false
      }
      fn(response)
    })
  }
}
