import { Notifications } from 'expo'

export class ExpoUploader {
  async upload (file) {
    this.uploadAlert = this.uploadAlert.bind(this)
    let localUri = file.uri
    let filename = file.name

    let match = /\.(\w+)$/.exec(filename)
    let type = match ? `image/${match[ 1 ]}` : `application/${file.name.split('.').pop()}`

    let formData = new FormData()
    formData.append('photo', { uri: localUri, name: filename, type })

    const response = await fetch('http://aplicativ0s.tmp.k8.com.br/teste.php?a=enviadopelonative13', {
      method: 'POST',
      body: formData,
      header: { 'content-type': 'multipart/form-data' }
    })
      .then(function (response) {
        return response.json()
      })

    this.uploadAlert(filename)
    return response
  }

  uploadAlert (filename) {
    const localNotification = {
      title: filename,
      body: 'Upload concluído',
      android: {
        icon: 'https://thumb1.shutterstock.com/display_pic_with_logo/2526499/444409879/stock-vector-download-sign-illustration-dark-gray-icon-on-transparent-background-444409879.jpg'
      }
    }
    const schedulingOptions = { time: (new Date()).getTime() + 10 }
    Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions)
  }
}
