import Upload from 'react-native-background-upload'
export class CreateAndroidUploader {
  upload (file) {
    let opt = { ...options }
    opt.path = file.path

    Upload.startUpload(opt).then((uploadId) => {
      Upload.addListener('progress', uploadId, (data) => {
        // console.log(`Progress: ${data.progress}%`)
      })
      Upload.addListener('error', uploadId, (data) => {
        console.log(`Error: ${data.error}%`)
      })
      Upload.addListener('cancelled', uploadId, (data) => {
        // console.log(`Cancelled!`)
      })
      Upload.addListener('completed', uploadId, (data) => {
        return data
      })
    }).catch((err) => {
      console.log('Upload error!', err)
    })
  }
}

var options = {
  url: 'http://aplicativ0s.tmp.k8.com.br/teste.php?a=enviadopelonative13',
  path: 'file://path/to/file/on/device',
  method: 'POST',
  field: 'uploaded_media',
  type: 'multipart',
  headers: {
    'my-custom-header': 's3headervalueorwhateveryouneed'
  },
  // Below are options only supported on Android
  notification: {
    enabled: true
  }
}
