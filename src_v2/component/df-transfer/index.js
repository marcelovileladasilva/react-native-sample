import { Platform } from 'react-native'
export class Transfer {
  constructor () {
    let Picker = null
    let Uploader = null
    // https://stackoverflow.com/questions/36367532/how-can-i-conditionally-import-an-es6-module

    if (!global.expo && Platform.OS === 'android') {
      // alert('Ops! Descomente o CreateAndroidPicker...')
      Picker = require('./CreateAndroidPicker').CreateAndroidPicker
      Uploader = require('./CreateAndroidUploader').CreateAndroidUploader
    } else {
      alert('Ops! Descomente o ExpoPicker...')
      // Esta comentado, pois a inclusao deste pacote impede a compilacao em projetos gerados pelo create-react-app
      // Picker = require('./ExpoPicker').ExpoPicker
      // Uploader = require('./ExpoUploader').ExpoUploader
    }
    this.picker = new Picker()
    this.uploader = new Uploader()
    // alert(this.picker.selectFileTapped)
  }

  selectFileTapped (fn) {
    this.picker.selectFileTapped(fn)
  }

  upload (file, fn) {
    return this.uploader.upload(file, fn)
  }
}
